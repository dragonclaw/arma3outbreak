if (hasInterface) then {
	PlayerLoadout = [];
	[] spawn {
		if (!isServer) then { [] execVM "base\actions.sqf"; };
		waitUntil { (player == player) };
		if (bstf_available) then { player setUnitLoadout bstfLoadout; };
		for "_i" from 0 to count allCurators -1 do { 
			[(allCurators select _i), [[player],true]] remoteExec ["addCuratorEditableObjects", 0];
		};
		player enableStamina false;
		player addEventHandler ["GetInMan", {[_this select 2] execVM "func\kp_fuel_consumption.sqf";}];
		waitUntil { !isNil "spawnPosition" };
		private _dir = random 360;
		private _pos = [5 + (random 3), _dir, 90, spawnPosition] call fnc_polToCart;
		player setPos [(_pos select 0) + ((random 6) - 3), (_pos select 1) + ((random 6) - 3), spawnPosition select 2];
		player setDir (_dir - 180);
	};
	[] spawn {
		waitUntil { (player == player) };
		while { true } do {
			waitUntil { !alive player };
			PlayerLoadout = getUnitLoadout [player, false];
			removeAllWeapons player;
			removeAllItems player;
			[] spawn {
				uisleep 2;
				if ((missionNamespace getVariable ["BIS_fnc_respawnTickets_valueWEST", 1]) <= 0) then {
					["mainObjective","FAILED"] call BIS_fnc_taskSetState;
					uisleep 3;
					["end2", true, true, true, true] remoteExec ["BIS_fnc_endMission"];
				};
			};
			waitUntil { alive player };
		};
	};
	with uiNamespace do {
		ui_zone = objNull;
		CashHudId = (["cashhud"] call BIS_fnc_rscLayer);
		CashHudId cutRsc ["RscCashHud", "PLAIN", 0, true];
		CashHudtext = (CashHud displayCtrl 4041);
		CashHudText ctrlSetStructuredText (parseText "Please Wait...");
		CashHudText ctrlCommit 0;
	};
	[] spawn {
		while { true } do {
			[] call fnc_updateCashHud;
			uisleep 1;
		};
	};
};
// Function ls_addToVehicle, add the options to play songs from a vehicle.
// Usage: [_vehicle] call fnc_ls_addToVehicle;
params ["_vehicle"];

private _locations = [[0, "ACE_MainActions"], [1, "ACE_SelfActions"]];
for "_l" from 0 to count _locations -1 do {
	private _locNum = (( _locations select _l) select 0);
	private _locName = (( _locations select _l) select 1);

	// Actions
	[_vehicle, _locNum, [_locName], ls_action_main]
		call fnc_addAceActionToObjectGlobal;
	[_vehicle, _locNum, [_locName, "ls_action_main"], ls_action_stop]
		call fnc_addAceActionToObjectGlobal;
	[_vehicle, _locNum, [_locName, "ls_action_main"], ls_action_volume]
		call fnc_addAceActionToObjectGlobal;

	// Volume
	for "_v" from 0 to count ls_volume_settings -1 do {
		[_vehicle, _locNum, [_locName, "ls_action_main", "ls_action_volume"], (ls_volume_settings select _v)]
			call fnc_addAceActionToObjectGlobal;
	};

	// Catagories
	for "_c" from 0 to count ls_sound_catagories -1 do {
		private _cat = (ls_sound_catagories select _c);
		private _catname = (_cat select 0);
		[_vehicle, _locNum, [_locName, "ls_action_main"], (_cat select 1)]
			call fnc_addAceActionToObjectGlobal;
		private _songs = (_cat select 2);
		for "_s" from 0 to count _songs -1 do {
			[_vehicle, _locNum, [_locName, "ls_action_main", _catname], (_songs select _s)]
				call fnc_addAceActionToObjectGlobal;
		};
	};
};
_vehicle addEventHandler ["Deleted", {
	params ["_unit"];
	if(!isNull (_unit getVariable ["ls_bottle", objNull])) then {
		deleteVehicle (_unit getVariable ["ls_bottle", objNull])
	};
}];
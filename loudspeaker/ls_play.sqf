// Function ls_play, play a song from a vehicle, controllable through ACE interactions.
// Usage: [_vehicle, _player, _song] call fnc_ls_play.sqf;
if (!isServer) exitWith {};
params ["_vehicle", "_player", "_song"];

if (isNull (_vehicle getVariable ["ls_bottle", objNull])) then {
	private _bottle = "Land_BottlePlastic_V2_F" createVehicle [0,0,1000];
	_bottle attachTo [_vehicle, [0, 0, 2]];
	_vehicle setVariable ["ls_bottle", _bottle, true];
	hideObjectGlobal _bottle;
	[_bottle, [_song, _vehicle getVariable ["ls_volume", 300], 1, false, 0]] remoteExec ["say3D", 0, false];
	[_vehicle, _bottle] spawn {
		params ["_vehicle", "_bottle"];
		private _volume = (_vehicle getVariable ["ls_volume", 300]);
		((allMissionObjects "RyanZombieCivilian_F") + (allMissionObjects "RyanZombieB_Soldier_base_F")) apply {
			if ((_vehicle distance _x) <= _volume) then {
				_x reveal [_vehicle, 4];
				_x setVariable ["RZ_Target",_vehicle];
				[_x,_vehicle] call RZ_fnc_zombie_engageTarget;
			};
		};
		while { !(isNull _bottle) } do {
			allUnits apply {
				if ((_vehicle distance _x) <= _volume) then {
					_x reveal [_vehicle, 4];
				};
			};
			sleep 10;
		};
	};
};
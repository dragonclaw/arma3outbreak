// Init the loudspeaker system.
// Usage: [] execVM "loudspeaker\ls_init.sqf";
// Functions
fnc_ls_addToVehicle = compileFinal preprocessFile "loudspeaker\ls_addToVehicle.sqf";
fnc_ls_play = compileFinal preprocessFile "loudspeaker\ls_play.sqf";

// Actions
ls_action_main = [
		"ls_action_main", "Loudspeaker", "", {},
		{ [_player, _target, []] call ace_common_fnc_canInteractWith },
		{}, [], {[0,0,0]}, 5,
	[false, false, false, false, false], {}];

ls_action_stop = [
		"ls_action_stop", "Stop", "",
		{ deleteVehicle (_target getVariable ["ls_bottle", objNull]) },
		{ (!isNull (_target getVariable ["ls_bottle", objNull])) },
		{}, [], {[0,0,0]}, 5,
	[false, false, false, false, false], {}];

ls_action_volume = [
		"ls_action_volume", "Change Volume", "",
		{ },
		{ (isNull (_target getVariable ["ls_bottle", objNull])) },
		{}, [], {[0,0,0]}, 5,
	[false, false, false, false, false], {}];

// Volume
private _ls_volume_text = [["One", 300], ["Two", 675], ["Three", 1000],
		["Four", 1400], ["Five", 1800], ["Eleven", 3000]];
ls_volume_settings = [];
for "_i" from count _ls_volume_text -1 to 0 step -1 do {
	private _volumetext = (_ls_volume_text select _i);
	private _setting = [format["ls_volume_%1", (_volumetext select 0)], (_volumetext select 0), "",
		{ _target setVariable ["ls_volume", ((_this select 2) select 0), true] },
		{ ((_target getVariable ["ls_volume", 300]) != ((_this select 2) select 0)) },
		{}, [(_volumetext select 1)], {[0,0,0]}, 2,
	[false, false, false, false, false], {}];
	ls_volume_settings pushBack _setting;
};

// Catagories - This links back to description.ext to link back to the files.
private _ls_text_catagories = [
	["Ace Combat", "acecombat", [
		["Alicorn", "ace7alicorn"],
		["Archange", "ace7archange"],
		["Daredevil", "ace7daredevil"],
		["The Unsung War", "ace5unsung"]
	] ],
	["Country", "country", [
		["Ain't I Right", "coaint"],
		["Big Iron", "cobig"],
		["Country Roads", "coroad"],
		["Hurt", "cohurt"]
	] ],
	["Dark Synth", "darksynth", [
		["Roller Mobster", "dsmob"],
		["Behemoth", "dsbehemoth"],
		["Corrupt By Design", "dscorrupt"],
		["Death Squad", "dsdeath"],
		["Disco Inferno", "dsdisco"],
		["Humans Are Such Easy Prey", "dshumans"]
	] ],
	["Films", "film", [
		["28 Days Later", "movie28days"],
		["A Bridge Too Far", "movieabridgetoofar"],
		["Fury", "movieapril1945"],
		["Band of Brothers", "moviebandofbrothers"],
		["The Matrix", "movieclubbed"],
		["Top Gun", "movietopgun"],
		["Danger Zone", "moviedangerzone"],
		["For A Few Dollars More", "movieafewdollars"],
		["Geralt Of Rivia", "moviegeralt"],
		["Toss a Coin to your Witcher", "movietosser"],
		["It's a Good Day to Die", "movieagoodday"],
		["Starship Troopers", "moviessttheme"],
		["The Good, the Bad and the Ugly", "moviegoodbadugly"],
		["Il Vento D'oro", "moviejojo"],
		["Now We Are Free", "moviegladiator"],
		["Komm Susser Todd", "movieevangelion"]
	] ],
	["Halo 3", "Halo 3", [
		["Finish the Fight", "halofinish"],
		["Never Forget", "halonever"],
		["One Final Effort", "halofinal"],
		["Roll Call", "halocall"]
	] ],
	["Metal", "metal", [
		["Attack of the Dead Men", "metaldeadmen"],
		["Hate Me", "metalhateme"],
		["In Flanders Fields", "metalflanders"],
		["BLYAT!", "milussr"]
	] ],
	["Rock", "rock", [
		["TNT", "acdctnt"],
		["Back in Black", "acdcback"],
		["Hell's Bells", "acdchellsbells"],
		["Highway to Hell", "acdchighway"],
		["Hail to the King", "rockhail"],
		["Bad Moon Rising", "rockbadmoon"],
		["Paranoid", "rockparanoid"],
		["War Pigs", "rockwarpigs"],
		["Celebrate", "rockcelebration"],
		["Moskau", "rockmoskau"],
		["Lemon Tree", "rocklemontree"],
		["Fortunate Son", "rockfortunateson"],
		["September", "rockseptember"],
		["Should I Stay or Should I Go?", "rockshouldistay"],
		["Down Under", "rockdownunder"]
	] ],
	["Pop", "pop", [
		["Ameno", "popameno"],
		["Boom Boom Boom Boom!!", "popboom"],
		["Caramell Dansen", "popdansen"],
		["I Need A Hero", "pophero"],
		["Pumped Up Kicks", "popkicks"],
		["FREE RTX 3090 CLICK HERE!!!!!", "poprick"],
		["Take My Breath Away", "popbreath"],
		["You Got That", "popugot"],
		["Run Boy Run", "poprun"]
	] ]
];
ls_sound_catagories = [];
for "_i" from 0 to count _ls_text_catagories -1 do {
	private _textcat = (_ls_text_catagories select _i);
	private _songs = [];
	private _catname = format["ls_cat_%1", (_textcat select 1)];
	private _cat = [_catname, (_textcat select 0), "", {},
		{ (isNull (_target getVariable ["ls_bottle", objNull])) },
		{}, [], {[0,0,0]}, 5,
	[false, false, false, false, false], {}];
	private _textsongs = (_textcat select 2);
	for "_j" from 0 to count _textsongs -1 do {
		private _textsong = (_textsongs select _j);
		private _song = [format["ls_song_%1", (_textsong select 1)], (_textsong select 0), "",
			{ [_target, _player, ((_this select 2) select 0)] remoteExec ["fnc_ls_play", 2]; },
			{ true }, {}, [(_textsong select 1)], {[0,0,0]}, 5,
		[false, false, false, false, false], {}];
		_songs pushBack _song;
	};
	ls_sound_catagories pushBack [_catname, _cat, _songs];
};

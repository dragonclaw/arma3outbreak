if (isServer) then { // Theoretically impossible to be false, but better safe than sorry?
	_hqwest = createCenter bluFor;
	_hqeast = createCenter opFor;
	_hqres = createCenter resistance;
	_hqciv = createCenter civilian;
	civilian setFriend [bluFor,1];
	civilian setFriend [opFor,1];
	civilian setFriend [independent,1];
	bluFor setFriend [civilian,1];
	opFor setFriend [civilian,1];
	independent setFriend [civilian,1];

	[] spawn {
		private _spawnPositions = [];
		private _zonePositions = [];
		private _arrows = allMissionObjects "Sign_Arrow_F";
		for "_i" from count _arrows -1 to 0 step -1 do {
			private _arrow = (_arrows select _i);
			switch (typeOf _arrow) do {
				case "Sign_Arrow_Blue_F": {
					_spawnPositions pushBack [(getPos _arrow), (getDir _arrow)];
				};
				case "Sign_Arrow_F": {
					private _zoneinfo = _arrow getVariable ["zoneinfo", ["Somewhere", 0, 0]];
					_zoneinfo pushback (getPos _arrow);
					_zonePositions pushBack _zoneinfo;
				};
				default {};
			};
			_arrow spawn { deleteVehicle _this; };
		};
		private _definitionScript = [] execVM "zone\definitions.sqf";
		waitUntil { scriptDone _definitionScript };
		[_zonePositions] execVM "zone\init.sqf";
		[_spawnPositions] execVM "base\init.sqf";
		[side_players, "mainObjective", ["Clear all infections!", "Cleanse Infection", "Cleanse"], objNull, "CREATED", 1, true, "defend", false] call BIS_fnc_taskCreate;
	};
	[] spawn {
		skipTime ((random 12) + 6);
		[0, (random 0.5) + 0.5, (random 20) - 10] remoteExec ["fnc_setWeather", 0, "setWeather"];
		while { true } do {
			private _changeTime = random 3600;
			[_changeTime, random 1, (random 20) - 10] remoteExec ["fnc_setWeather", 0, "setWeather"];
			sleep _changeTime;
		};
	};

	["ace_captiveStatusChanged", {
		params ["_unit", "_state", "_reason", "_caller"];
		if ((side group _unit) == side_looters && (_state && _reason == "SetHandcuffed")) then {
			_unit spawn {
				sleep (59 + ceil (random 61));
				deleteVehicle _this;
			};
		};
	}] call CBA_fnc_addEventHandler;

	[west, -1, [
		// Sandy Sandbags
		["Land_BagFence_Long_F", -1], ["Land_BagFence_Short_F", -1], ["Land_BagFence_Round_F", -1],
		// Grassy Sandbags
		//["Land_BagFence_01_long_green_F", -1], ["Land_BagFence_01_short_green_F", -1], ["Land_BagFence_01_round_green_F", -1],
		["Land_Razorwire_F", -1], ["Land_CncBarrier_stripes_F", -1],
		["Land_SignWarning_01_CheckpointAhead_F", -1], ["Land_SignM_WarningMilAreaSmall_english_F", -1],
		//["Land_PlasticNetFence_01_short_F", -1], ["Land_PlasticNetFence_01_long_F", -1], ["Land_PlasticNetFence_01_pole_F", -1],
		["Land_Plank_01_4m_F", -1], ["Land_Plank_01_8m_F", -1], ["Land_Campfire_F", -1], ["Land_CampingChair_V1_F", -1],
		["Land_PortableLight_double_F", -1], ["Land_PortableHelipadLight_01_F", -1], ["Land_HelipadSquare_F", -1] ]] call ace_fortify_fnc_registerObjects;
};
if (!isMultiplayer) then {
	private _units = switchableUnits;
	for "_i" from (count _units) -1 to 0 step -1 do {
		if ((_units select _i) != player) then {
			deleteVehicle (_units select _i);
		};
	};
	player assignCurator (allCurators select 0);
};
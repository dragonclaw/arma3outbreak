fnc_base_becomeClass = compileFinal preprocessFile "base\func\becomeClass.sqf";
fnc_base_deploySpawn = compileFinal preprocessFile "base\func\deploySpawn.sqf";
fnc_base_deploymedical = compileFinal preprocessFile "base\func\deployMedical.sqf";
fnc_base_containerize = compileFinal preprocessFile "base\func\containerize.sqf";

if (!isServer) exitWith {};
params ["_spawnPositions"];

base_tick_divisor = 20;

private _actionScript = [] execVM "base\actions.sqf";
waitUntil { scriptDone _actionScript };

[_spawnPositions] execVM "base\func\startSpawn.sqf";

// Give players $3,000 pocket money for starting the mission!
// And $100 for each additional player in the mission.
private _bonus = 0;
if (isMultiplayer) then { _bonus = ((count playableUnits) - 1) * 100; };
[(3000 + _bonus)] call fnc_base_earncash;

// And 10 respawn tickets!
[side_players, 10] call BIS_fnc_respawnTickets;

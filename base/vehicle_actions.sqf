// Vehicles
base_action_buy_vehicle = ["action_buy_vehicle", "Vehicle", "", {},
	{ [_player, _target, []] call ace_common_fnc_canInteractWith },
	{}, [], {[0, 0, 0]}, 2, [false, false, false, false, false], {}];
base_action_buy_helicopter = ["action_buy_helicopter", "Helicopter", "", {},
	{ [_player, _target, []] call ace_common_fnc_canInteractWith },
	{}, [], {[0, 0, 0]}, 2, [false, false, false, false, false], {}];
base_action_buy_tank = ["action_buy_tank", "Tank", "", {},
	{ [_player, _target, []] call ace_common_fnc_canInteractWith },
	{}, [], {[0, 0, 0]}, 2, [false, false, false, false, false], {}];
base_action_buy_drone = ["action_buy_drone", "Drone", "", {},
	{ [_player, _target, []] call ace_common_fnc_canInteractWith && (_player getVariable ["class", "_"]) == "jtac" },
	{}, [], {[0, 0, 0]}, 2, [false, false, false, false, false], {}];

base_actions_buy_vehicles = [
	["action_buy_vehicle", [
		["offroad", "Offroad", 200, [
			["offroad_mg", "Offroad M2", 800],
			["offroad_at", "Offroad SPG", 1400]
		]],
		["hunter", "Hunter", 2600, [
			["hunter_mg", "Hunter HMG", 3600]
		]],
		["hemtt", "HEMTT", 1800, [
			["hemtt_ammo", "HEMTT (Ammo)", 3200],
			["hemtt_repair", "HEMTT (Supply)", 2400],
			["hemtt_medical", "HEMTT (Medical)", 3200]
		]]
	]],
	["action_buy_helicopter", [
		["sparrow", "MH-6 Little Bird", 2400, [
			["sparrow_at", "AH-6 Little Bird", 5600]
		]],
		["comanche", "RAH-66 Comanche", 18000, []],
		["ghosthawk", "Ghost Hawk", 2800, []],
		["taru", "Taru", 3800, [
			["taru_repair", "Taru (Supply)", 12000],
			["taru_medical", "Taru (Medical)", 24000]
		]]
	]],
	["action_buy_tank", [
		["mlrs", "Seara MLRS", 7600, []],
		["scorcher", "Sholef", 9000, []],
		["mbt", "Merkava", 15000, []],
		["badger", "Badger", 16000, []]
	]],
	["action_buy_drone", [
		["stomper", "Stomper", 1500, [
			["stomper_mg", "Stomper HMG", 5000]
		]],
		["yahbon", "Yahbon R3", 3200, []],
		["falcon", "MQ-12 Falcon", 4400, []]
	]]
];

if (isServer) then {
	// Cars
	fnc_spawn_offroad = {
		params["_target", "_player"];
		private _vehicle = "B_G_Offroad_01_F" createVehicle base_vehicle_spawn_pos;
		_vehicle setVariable ["boxname", "Offroad", true];
		[_target, _vehicle] call fnc_setupVehicle;
		[_vehicle, ["Green",1], ["HideDoor1",0,"HideDoor2",0,"HideDoor3",0,"HideBackpacks",1,"HideBumper1",1,"HideBumper2",0,"HideConstruction",0,"hidePolice",1,"HideServices",1,"BeaconsStart",0,"BeaconsServicesStart",0]] call BIS_fnc_initVehicle;
		_vehicle setObjectTextureGlobal [0,"\a3\Soft_F_Enoch\Offroad_01\Data\offroad_01_ext_blk_CO.paa"];
	};
	fnc_spawn_offroad_mg = {
		params["_target", "_player"];
		private _vehicle = "B_G_Offroad_01_armed_F" createVehicle base_vehicle_spawn_pos;
		_vehicle setVariable ["boxname", "Offroad M2", true];
		[_target, _vehicle] call fnc_setupVehicle;
		[_vehicle, ["Green",1], ["HideDoor1",0,"HideDoor2",0,"HideDoor3",0,"HideBackpacks",1,"HideBumper1",1,"HideBumper2",0,"HideConstruction",0,"hidePolice",1,"HideServices",1,"BeaconsStart",0,"BeaconsServicesStart",0]] call BIS_fnc_initVehicle;
		_vehicle setObjectTextureGlobal [0,"\a3\Soft_F_Enoch\Offroad_01\Data\offroad_01_ext_blk_CO.paa"];
	};
	fnc_spawn_offroad_at = {
		params["_target", "_player"];
		private _vehicle = "B_G_Offroad_01_AT_F" createVehicle base_vehicle_spawn_pos;
		_vehicle setVariable ["boxname", "Offroad SPG", true];
		[_target, _vehicle] call fnc_setupVehicle;
		[_vehicle, ["Green",1], ["HideDoor1",0,"HideDoor2",0,"HideDoor3",0,"HideBackpacks",1,"HideBumper1",1,"HideBumper2",0,"HideConstruction",0,"hidePolice",1,"HideServices",1,"BeaconsStart",0,"BeaconsServicesStart",0]] call BIS_fnc_initVehicle;
		_vehicle setObjectTextureGlobal [0,"\a3\Soft_F_Enoch\Offroad_01\Data\offroad_01_ext_blk_CO.paa"];
	};
	fnc_spawn_hunter = {
		params["_target", "_player"];
		private _vehicle = "B_MRAP_01_F" createVehicle base_vehicle_spawn_pos;
		_vehicle setVariable ["boxname", "Hunter", true];
		[_target, _vehicle] call fnc_setupVehicle;
		_vehicle setObjectTextureGlobal [0,"\dc\bstf\data\dc_mrap_01_bstf_co.paa"];
		_vehicle setObjectTextureGlobal [1,"\dc\bstf\data\dc_mrap_01_adds_bstf_co.paa"];
		_vehicle setObjectTextureGlobal [2,"\dc\bstf\data\dc_turret_bstf_co.paa"];
	};
	fnc_spawn_hunter_mg = {
		params["_target", "_player"];
		private _vehicle = "B_MRAP_01_hmg_F" createVehicle base_vehicle_spawn_pos;
		_vehicle setVariable ["boxname", "Hunter HMG", true];
		[_target, _vehicle] call fnc_setupVehicle;
		_vehicle setObjectTextureGlobal [0,"\dc\bstf\data\dc_mrap_01_bstf_co.paa"];
		_vehicle setObjectTextureGlobal [1,"\dc\bstf\data\dc_mrap_01_adds_bstf_co.paa"];
		_vehicle setObjectTextureGlobal [2,"\dc\bstf\data\dc_turret_bstf_co.paa"];
	};
	// Helicopters
	fnc_spawn_sparrow = {
		params["_target", "_player"];
		private _vehicle = "B_Heli_Light_01_F" createVehicle base_vehicle_spawn_pos;
		_vehicle setVariable ["boxname", "MH-6 Little Bird", true];
		[_target, _vehicle] call fnc_setupVehicle;
		_vehicle setObjectTextureGlobal [0,"\a3\air_f\Heli_Light_01\Data\heli_light_01_ext_ion_co.paa"];
	};
	fnc_spawn_sparrow_at = {
		params["_target", "_player"];
		private _vehicle = "B_Heli_Light_01_dynamicLoadout_F" createVehicle base_vehicle_spawn_pos;
		_vehicle setVariable ["boxname", "AH-6 Little Bird", true];
		[_target, _vehicle] call fnc_setupVehicle;
		_vehicle setObjectTextureGlobal [0,"\a3\air_f\Heli_Light_01\Data\heli_light_01_ext_ion_co.paa"];
	};
	fnc_spawn_comanche = {
		params["_target", "_player"];
		private _vehicle = "B_Heli_Attack_01_dynamicLoadout_F" createVehicle base_vehicle_spawn_pos;
		_vehicle setVariable ["boxname", "RAH-66 Comanche", true];
		[_target, _vehicle] call fnc_setupVehicle;
		_vehicle setObjectTextureGlobal [0,"\dc\bstf\Data\dc_heli_attack_01_bstf_co.paa"];
	};
	fnc_spawn_ghosthawk = {
		params["_target", "_player"];
		private _vehicle = "B_Heli_Transport_01_F" createVehicle base_vehicle_spawn_pos;
		_vehicle setVariable ["boxname", "Ghost Hawk", true];
		[_target, _vehicle] call fnc_setupVehicle;
	};
	fnc_spawn_taru = {
		params["_target", "_player"];
		private _vehicle = "O_Heli_Transport_04_F" createVehicle base_vehicle_spawn_pos;
		_vehicle setVariable ["boxname", "Taru", true];
		[_target, _vehicle] call fnc_setupVehicle;
		_vehicle setObjectTextureGlobal [0,"\A3\Air_F_Heli\Heli_Transport_04\Data\heli_transport_04_base_01_Black_co.paa"];
		_vehicle setObjectTextureGlobal [1,"\A3\Air_F_Heli\Heli_Transport_04\Data\heli_transport_04_base_02_Black_co.paa"];
	};
	fnc_spawn_taru_repair = {
		params["_target", "_player"];
		private _vehicle = "O_Heli_Transport_04_repair_F" createVehicle base_vehicle_spawn_pos;
		_vehicle setVariable ["boxname", "Taru (Repair)", true];
		[_target, _vehicle] call fnc_setupVehicle;
		_vehicle setObjectTextureGlobal [0,"\A3\Air_F_Heli\Heli_Transport_04\Data\heli_transport_04_base_01_Black_co.paa"];
		_vehicle setObjectTextureGlobal [1,"\A3\Air_F_Heli\Heli_Transport_04\Data\heli_transport_04_base_02_Black_co.paa"];
		_vehicle setObjectTextureGlobal [2,"\A3\Air_F_Heli\Heli_Transport_04\Data\Heli_Transport_04_Pod_Ext01_Black_CO.paa"];
		_vehicle setObjectTextureGlobal [3,"\A3\Air_F_Heli\Heli_Transport_04\Data\Heli_Transport_04_Pod_Ext02_Black_CO.paa"];
		[_vehicle, 10000, [[-1.52,1.14,-1.18]]] call ace_refuel_fnc_makeSource;
		_vehicle setVariable ["ace_rearm_isSupplyVehicle", true, true];
		_vehicle setVariable ["rearm", ([_vehicle] remoteExec ["ace_rearm_fnc_initSupplyVehicle", 0, true])];
		[_vehicle, true] call ace_arsenal_fnc_initBox;
	};
	fnc_spawn_taru_medical = {
		params["_target", "_player"];
		private _vehicle = "O_Heli_Transport_04_medevac_F" createVehicle base_vehicle_spawn_pos;
		_vehicle setVariable ["boxname", "Taru (Medical)", true];
		[_target, _vehicle] call fnc_setupVehicle;
		_vehicle setObjectTextureGlobal [0,"\A3\Air_F_Heli\Heli_Transport_04\Data\heli_transport_04_base_01_Black_co.paa"];
		_vehicle setObjectTextureGlobal [1,"\A3\Air_F_Heli\Heli_Transport_04\Data\heli_transport_04_base_02_Black_co.paa"];
		_vehicle setObjectTextureGlobal [2,"\A3\Air_F_Heli\Heli_Transport_04\Data\Heli_Transport_04_Pod_Ext01_Black_CO.paa"];
		_vehicle setObjectTextureGlobal [3,"\A3\Air_F_Heli\Heli_Transport_04\Data\Heli_Transport_04_Pod_Ext02_Black_CO.paa"];
		[_vehicle, true] call ace_arsenal_fnc_initBox;
		[_vehicle, 0, ["ACE_MainActions"], base_action_deploy_medical]
			call fnc_addAceActionToObjectGlobal;
	};
	// Trucks
	fnc_spawn_hemtt = {
		params["_target", "_player"];
		private _vehicle = "B_Truck_01_transport_F" createVehicle base_vehicle_spawn_pos;
		_vehicle setVariable ["boxname", "HEMTT", true];
		[_target, _vehicle] call fnc_setupVehicle;
		_vehicle setObjectTextureGlobal [0,"\dc\bstf\data\dc_truck_01_ext_01_bstf_co.paa"];
		_vehicle setObjectTextureGlobal [1,"\dc\bstf\data\dc_truck_01_ext_02_bstf_co.paa"];
		_vehicle setObjectTextureGlobal [2,"\dc\bstf\data\dc_truck_01_cargo_bstf_co.paa"];
		_vehicle setObjectTextureGlobal [3,"\dc\bstf\data\dc_truck_01_cover_bstf_co.paa"];
		private _gmg = "B_UGV_01_rcws_F" createVehicle base_vehicle_spawn_pos;
		[_gmg, (createGroup (side _player))] call BIS_fnc_spawnCrew;
		_gmg allowDamage false;
		_gmg setObjectTextureGlobal [0,""];
		_gmg setObjectTextureGlobal [1,""];
		_gmg setObjectTextureGlobal [2,"\dc\bstf\data\dc_turret_bstf_co.paa"];
		_gmg disableAI "LIGHTS";
		_gmg lockCargo true;
		_gmg lockDriver true;
		_gmg lockInventory true;
		_gmg lock true;
		_gmg setFuel 0;
		_gmg setHit ["hitlfwheel", 0.5];
		_gmg setHit ["hitrfwheel", 0.5];
		_gmg setHit ["hitlf2wheel", 0.5];
		_gmg setHit ["hitrf2wheel", 0.5];
		_gmg setHit ["hitlmwheel", 0.5];
		_gmg setHit ["hitrmwheel", 0.5];
		deleteVehicle (driver _gmg);
		_gmg attachTo [_vehicle, [-0.4, 3.5, 1.118]];
		_gmg setDir 0;
		private _sat = "SatelliteAntenna_01_Small_Mounted_Black_F" createVehicle [-1000,-1000, (random 100 + 2000)];
		_sat attachTo [_vehicle, [-1.45,1.1,0.3]];
		_sat setDir 270;
		_sat enableSimulation false;
		private _phone = "Land_IPPhone_01_black_F" createVehicle [-1000,-1000, (random 100 + 2000)];
		_phone attachTo [_vehicle, [-1.18,1.1,-0.5]];
		_phone setDir 90;
		[_phone,90,270] call BIS_fnc_setPitchBank;
		_phone enableSimulation false;
		private _ant = "OmniDirectionalAntenna_01_black_F" createVehicle [-1000,-1000, (random 100 + 2000)];
		_ant attachTo [_vehicle, [0.36, 1.6, 1.9]];
		_ant setDir 0;
		_ant enableSimulation false;
		_vehicle addEventHandler ["Killed", {
			if (isServer) then {
				params ["_vehicle", "_killer", "_instigator", "_useEffects"];
				{
					_x allowDamage true;
					_x setDamage 1;
				} forEach (attachedObjects _vehicle);
			};
		}];
		_vehicle addEventHandler ["Deleted", {
			if (isServer) then {
				params ["_vehicle"];
				{ deleteVehicle _x } forEach (attachedObjects _vehicle);
			};
		}];
	};
	fnc_spawn_hemtt_ammo = {
		params["_target", "_player"];
		private _vehicle = "B_Truck_01_ammo_F" createVehicle base_vehicle_spawn_pos;
		_vehicle setVariable ["boxname", "HEMTT (Ammo)", true];
		[_target, _vehicle] call fnc_setupVehicle;
		_vehicle setObjectTextureGlobal [0,"\dc\bstf\data\dc_truck_01_ext_01_bstf_co.paa"];
		_vehicle setObjectTextureGlobal [1,"\dc\bstf\data\dc_truck_01_ext_02_bstf_co.paa"];
		_vehicle setObjectTextureGlobal [2,"\dc\bstf\data\dc_truck_01_ammo_bstf_co.paa"];
		private _gmg = "B_UGV_01_rcws_F" createVehicle base_vehicle_spawn_pos;
		[_gmg, (createGroup (side _player))] call BIS_fnc_spawnCrew;
		_gmg allowDamage false;
		_gmg setObjectTextureGlobal [0,""];
		_gmg setObjectTextureGlobal [1,""];
		_gmg setObjectTextureGlobal [2,"\dc\bstf\data\dc_turret_bstf_co.paa"];
		_gmg disableAI "LIGHTS";
		_gmg lockCargo true;
		_gmg lockDriver true;
		_gmg lockInventory true;
		_gmg lock true;
		_gmg setFuel 0;
		_gmg setHit ["hitlfwheel", 0.5];
		_gmg setHit ["hitrfwheel", 0.5];
		_gmg setHit ["hitlf2wheel", 0.5];
		_gmg setHit ["hitrf2wheel", 0.5];
		_gmg setHit ["hitlmwheel", 0.5];
		_gmg setHit ["hitrmwheel", 0.5];
		deleteVehicle (driver _gmg);
		_gmg attachTo [_vehicle, [-0.4,3.45,1.412]];
		_gmg setDir 0;
		private _sat = "SatelliteAntenna_01_Small_Mounted_Black_F" createVehicle [-1000,-1000, (random 100 + 2000)];
		_sat attachTo [_vehicle, [-1.455,0.923,0.6]];
		_sat setDir 270;
		_sat enableSimulation false;
		private _phone = "Land_IPPhone_01_black_F" createVehicle [-1000,-1000, (random 100 + 2000)];
		_phone attachTo [_vehicle, [-1.2,1,-0.2]];
		_phone setDir 90;
		[_phone,90,270] call BIS_fnc_setPitchBank;
		_phone enableSimulation false;
		private _ant = "OmniDirectionalAntenna_01_black_F" createVehicle [-1000,-1000, (random 100 + 2000)];
		_ant attachTo [_vehicle, [0.5, 1.5, 2.2]];
		_ant setDir 0;
		_ant enableSimulation false;
		_vehicle addEventHandler ["Killed", {
			if (isServer) then {
				params ["_vehicle", "_killer", "_instigator", "_useEffects"];
				{
					_x allowDamage true;
					_x setDamage 1;
				} forEach (attachedObjects _vehicle);
			};
		}];
		_vehicle addEventHandler ["Deleted", {
			if (isServer) then {
				params ["_vehicle"];
				{ deleteVehicle _x } forEach (attachedObjects _vehicle);
			};
		}];
		private _action = { ["Open",true] call BIS_fnc_arsenal; };
		if (ace_available) then {
				[_vehicle, true] call ace_arsenal_fnc_initBox;
				_action = {
					params ["_target", "_caller", "_actionId", "_arguments"];
					[_target, _caller] call ace_arsenal_fnc_openBox;
				};
		};
		[_vehicle, [localize "STR_A3_Arsenal", _action, nil, 1.5, true, true, "", "true", 3, false, "", ""]]
			call fnc_addActionGlobal;
		[_vehicle, ["Arm Survivors ($500)", {
			params ["_target", "_caller", "_actionId", "_arguments"];
			private _zone = ([getPos _target] call fnc_inZone);
			if ([500] call fnc_base_spendcash) then {
				[Announcer, (format["%1 armed the survivors of %2 for $%3!", name _caller, _zone getVariable "name", 500])] remoteExec ["globalChat", 0];
				_zone setVariable ["survivorguns", true, true];
			};
		}, nil, 1.4, true, true, "", "(missionNamespace getVariable ['base_cash', 0]) >= 500 && !(isNull ([getPos _target] call fnc_inZone)) && !(([getPos _target] call fnc_inZone) getVariable ['survivorguns', false])", 3, false, "", ""]]
			call fnc_addActionGlobal;
	};
	fnc_spawn_hemtt_fuel = {
		params["_target", "_player"];
		private _vehicle = "B_Truck_01_fuel_F" createVehicle base_vehicle_spawn_pos;
		_vehicle setVariable ["boxname", "HEMTT (Fuel)", true];
		[_target, _vehicle] call fnc_setupVehicle;
		_vehicle setObjectTextureGlobal [0,"\dc\bstf\data\dc_truck_01_ext_01_bstf_co.paa"];
		_vehicle setObjectTextureGlobal [1,"\dc\bstf\data\dc_truck_01_ext_02_bstf_co.paa"];
		_vehicle setObjectTextureGlobal [2,"\dc\bstf\data\dc_truck_01_fuel_bstf_co.paa"];
	};
	fnc_spawn_hemtt_repair = {
		params["_target", "_player"];
		private _vehicle = "B_Truck_01_fuel_F" createVehicle base_vehicle_spawn_pos;
		_vehicle setVariable ["boxname", "HEMTT (Supply)", true];
		[_target, _vehicle] call fnc_setupVehicle;
		_vehicle setObjectTextureGlobal [0,"\dc\bstf\data\dc_truck_01_ext_01_bstf_co.paa"];
		_vehicle setObjectTextureGlobal [1,"\dc\bstf\data\dc_truck_01_ext_02_bstf_co.paa"];
		_vehicle setObjectTextureGlobal [2,"\dc\bstf\data\dc_truck_01_fuel_bstf_co.paa"];
		_vehicle setVariable ["ACE_isRepairVehicle", true, true];
		[_vehicle, 10000, [[0.28,-4.99,-0.3],[-0.25,-4.99,-0.3]]] call ace_refuel_fnc_makeSource;
		_vehicle setVariable ["ace_rearm_isSupplyVehicle", true, true];
		_vehicle setVariable ["rearm", ([_vehicle] remoteExec ["ace_rearm_fnc_initSupplyVehicle", 0, true])];
		private _sat = "SatelliteAntenna_01_Small_Mounted_Black_F" createVehicle [-1000,-1000, (random 100 + 2000)];
		_sat attachTo [_vehicle, [-1.455,0.923,0.6]];
		_sat setDir 270;
		_sat enableSimulation false;
		private _phone = "Land_IPPhone_01_black_F" createVehicle [-1000,-1000, (random 100 + 2000)];
		_phone attachTo [_vehicle, [-1.2,1,-0.2]];
		_phone setDir 90;
		[_phone,90,270] call BIS_fnc_setPitchBank;
		_phone enableSimulation false;
		private _ant = "OmniDirectionalAntenna_01_black_F" createVehicle [-1000,-1000, (random 100 + 2000)];
		_ant attachTo [_vehicle, [0.5, 1.5, 2.2]];
		_ant setDir 0;
		_ant enableSimulation false;
		_vehicle addEventHandler ["Killed", {
			if (isServer) then {
				params ["_vehicle", "_killer", "_instigator", "_useEffects"];
				{
					_x allowDamage true;
					_x setDamage 1;
				} forEach (attachedObjects _vehicle);
			};
		}];
		_vehicle addEventHandler ["Deleted", {
			if (isServer) then {
				params ["_vehicle"];
				{ deleteVehicle _x } forEach (attachedObjects _vehicle);
			};
		}];
	};
	fnc_spawn_hemtt_medical = {
		params["_target", "_player"];
		private _vehicle = "B_Truck_01_medical_F" createVehicle base_vehicle_spawn_pos;
		_vehicle setVariable ["boxname", "HEMTT (Medical)", true];
		[_target, _vehicle] call fnc_setupVehicle;
		_vehicle setObjectTextureGlobal [0,"\dc\bstf\data\dc_truck_01_ext_01_bstf_co.paa"];
		_vehicle setObjectTextureGlobal [1,"\dc\bstf\data\dc_truck_01_ext_02_bstf_co.paa"];
		_vehicle setObjectTextureGlobal [2,"\dc\bstf\data\dc_truck_01_cargo_bstf_co.paa"];
		_vehicle setObjectTextureGlobal [3,"\dc\bstf\data\dc_truck_01_cover_bstf_co.paa"];
		private _gmg = "B_UGV_01_rcws_F" createVehicle base_vehicle_spawn_pos;
		_gmg allowDamage false;
		[_gmg, (createGroup (side _player))] call BIS_fnc_spawnCrew;
		_gmg setObjectTextureGlobal [0,""];
		_gmg setObjectTextureGlobal [1,""];
		_gmg setObjectTextureGlobal [2,"\dc\bstf\data\dc_turret_bstf_co.paa"];
		_gmg disableAI "LIGHTS";
		_gmg lockCargo true;
		_gmg lockDriver true;
		_gmg lockInventory true;
		_gmg lock true;
		_gmg setFuel 0;
		_gmg setHit ["hitlfwheel", 0.5];
		_gmg setHit ["hitrfwheel", 0.5];
		_gmg setHit ["hitlf2wheel", 0.5];
		_gmg setHit ["hitrf2wheel", 0.5];
		_gmg setHit ["hitlmwheel", 0.5];
		_gmg setHit ["hitrmwheel", 0.5];
		deleteVehicle (driver _gmg);
		_gmg attachTo [_vehicle, [-0.613, 3.5, 1.118]];
		_gmg setDir 0;
		private _sat = "SatelliteAntenna_01_Small_Mounted_Black_F" createVehicle [-1000,-1000, (random 100 + 2000)];
		_sat attachTo [_vehicle, [-1.63,1.13,0.3]];
		_sat setDir 270;
		_sat enableSimulation false;
		private _phone = "Land_IPPhone_01_black_F" createVehicle [-1000,-1000, (random 100 + 2000)];
		_phone attachTo [_vehicle, [-1.36,1.1,-0.5]];
		_phone setDir 90;
		[_phone,90,270] call BIS_fnc_setPitchBank;
		_phone enableSimulation false;
		private _ant = "OmniDirectionalAntenna_01_black_F" createVehicle [-1000,-1000, (random 100 + 2000)];
		_ant attachTo [_vehicle, [0.34, 1.6, 2]];
		_ant setDir 0;
		_ant enableSimulation false;
		_vehicle addEventHandler ["Killed", {
			if (isServer) then {
				params ["_vehicle", "_killer", "_instigator", "_useEffects"];
				{
					_x allowDamage true;
					_x setDamage 1;
				} forEach (attachedObjects _vehicle);
			};
		}];
		_vehicle addEventHandler ["Deleted", {
			if (isServer) then {
				params ["_vehicle"];
				{ deleteVehicle _x } forEach (attachedObjects _vehicle);
			};
		}];
		if (ace_available) then {
			[_vehicle, 0, ["ACE_MainActions"], base_action_deploy_medical]
				call fnc_addAceActionToObjectGlobal;
		};
		[_vehicle, ["Deploy", {
			params ["_target", "_caller", "_actionId", "_arguments"];
			[_target, _caller] remoteExec ["fnc_base_deploymedical", 2];
		}, nil, 1.4, true, true, "", "(({ side _x != civilian } count (crew _target)) == 0) && !(isNull ([getPos _target] call fnc_inZone)) && isNull (([getPos _target] call fnc_inZone) getVariable ['medical', objNull])", 3, false, "", ""]]
			call fnc_addActionGlobal;
	};
	// Armor
	fnc_spawn_mlrs = {
		params["_target", "_player"];
		private _vehicle = "B_MBT_01_mlrs_F" createVehicle base_vehicle_spawn_pos;
		_vehicle setVariable ["boxname", "Seara MLRS", true];
		[_target, _vehicle] call fnc_setupVehicle;
		_vehicle setObjectTextureGlobal [0,"\dc\bstf\data\dc_mbt_01_body_bstf_co.paa"];
		_vehicle setObjectTextureGlobal [1,"\dc\bstf\data\dc_mbt_01_mlrs_bstf_co.paa"];
	};
	fnc_spawn_scorcher = {
		params["_target", "_player"];
		private _vehicle = "B_MBT_01_arty_F" createVehicle base_vehicle_spawn_pos;
		_vehicle setVariable ["boxname", "Sholef", true];
		[_target, _vehicle] call fnc_setupVehicle;
		_vehicle setObjectTextureGlobal [0,"\dc\bstf\data\dc_mbt_01_body_bstf_co.paa"];
		_vehicle setObjectTextureGlobal [1,"\dc\bstf\data\dc_mbt_01_scorcher_bstf_co.paa"];
		_vehicle setObjectTextureGlobal [2,"\dc\bstf\data\dc_turret_bstf_co.paa"];
	};
	fnc_spawn_mbt = {
		params["_target", "_player"];
		private _vehicle = "B_MBT_01_TUSK_F" createVehicle base_vehicle_spawn_pos;
		_vehicle setVariable ["boxname", "Merkava MBT", true];
		[_target, _vehicle] call fnc_setupVehicle;
		_vehicle setObjectTextureGlobal [0,"\dc\bstf\data\dc_mbt_01_body_bstf_co.paa"];
		_vehicle setObjectTextureGlobal [1,"\dc\bstf\data\dc_mbt_01_tow_bstf_co.paa"];
		_vehicle setObjectTextureGlobal [2,"\dc\bstf\data\dc_mbt_addons_bstf_co.paa"];
	};
	fnc_spawn_badger = {
		params["_target", "_player"];
		private _vehicle = "B_APC_Wheeled_01_cannon_F" createVehicle base_vehicle_spawn_pos;
		_vehicle setVariable ["boxname", "Badger", true];
		[_target, _vehicle] call fnc_setupVehicle;
		_vehicle setObjectTextureGlobal [0,"\dc\bstf\data\dc_apc_wheeled_01_base_bstf_co.paa"];
		_vehicle setObjectTextureGlobal [1,"\dc\bstf\data\dc_apc_wheeled_01_adds_bstf_co.paa"];
		_vehicle setObjectTextureGlobal [2,"\dc\bstf\data\dc_apc_wheeled_01_tows_bstf_co.paa"];
	};
	// UAVs
	fnc_spawn_stomper = {
		params["_target", "_player"];
		private _vehicle = "B_UGV_01_F" createVehicle base_vehicle_spawn_pos;
		_vehicle setVariable ["boxname", "UGV Stomper", true];
		[_target, _vehicle, "jtac"] call fnc_setupVehicle;
		_vehicle setObjectTextureGlobal [0,"\dc\bstf\data\dc_ugv_01_ext_bstf_co.paa"];
		_vehicle setObjectTextureGlobal [1,"\dc\bstf\data\dc_ugv_01_int_bstf_co.paa"];
		_vehicle setObjectTextureGlobal [2,"\dc\bstf\data\dc_turret_bstf_co.paa"];
		[_vehicle, true] call ace_arsenal_fnc_initBox;
	};
	fnc_spawn_stomper_mg = {
		params["_target", "_player"];
		private _vehicle = "B_UGV_01_rcws_F" createVehicle base_vehicle_spawn_pos;
		_vehicle setVariable ["boxname", "UGV Stomper HMG", true];
		[_target, _vehicle, "jtac"] call fnc_setupVehicle;
		_vehicle setObjectTextureGlobal [0,"\dc\bstf\data\dc_ugv_01_ext_bstf_co.paa"];
		_vehicle setObjectTextureGlobal [1,"\dc\bstf\data\dc_ugv_01_int_bstf_co.paa"];
		_vehicle setObjectTextureGlobal [2,"\dc\bstf\data\dc_turret_bstf_co.paa"];
	};
	fnc_spawn_yahbon = {
		params["_target", "_player"];
		private _vehicle = "B_UAV_02_dynamicLoadout_F" createVehicle base_vehicle_spawn_pos;
		_vehicle setVariable ["boxname", "R3 Yahbon", true];
		[_target, _vehicle, "jtac"] call fnc_setupVehicle;
	};
	fnc_spawn_falcon = {
		params["_target", "_player"];
		private _vehicle = "B_T_UAV_03_dynamicLoadout_F" createVehicle base_vehicle_spawn_pos;
		_vehicle setVariable ["boxname", "MQ-12 Falcon", true];
		[_target, _vehicle, "jtac"] call fnc_setupVehicle;
	};
	// Standardized Setup
	fnc_setupVehicle = {
		params ["_target", "_vehicle", ["_class", "_"]];
		// Uncomment here to have the new box loaded into the cargo of the object that spawned it.
		//private _box = [_vehicle, getPos (_target getVariable "repairspot"), _class] call fnc_base_containerize;
		//[_box, _target, true] call ace_cargo_fnc_loadItem;
		private _pos = getPos (_target getVariable "repairspot");
		_vehicle setDir (getDir (_target getVariable "depot"));
		_vehicle setPos [_pos select 0, _pos select 1, 0];
		[_vehicle] call fnc_ls_addToVehicle;
		clearItemCargoGlobal _vehicle;
		clearMagazineCargoGlobal _vehicle;
		clearWeaponCargoGlobal _vehicle;
		clearBackpackCargoGlobal _vehicle;
		for "_i" from 0 to count allCurators -1 do { 
			(allCurators select _i) addCuratorEditableObjects [[_vehicle],TRUE]; 
		};
	};
};
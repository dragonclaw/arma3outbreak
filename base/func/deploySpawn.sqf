// Should only be called by deleting the spawn vehicle.
// Usage: [] spawn fnc_deploySpawn
// Returns: Nothing.
if (!isServer) exitWith {};
params ["_spawnPos", "_spawnDir", "_spawnTent"];
_spawnPos set [2, (_spawnPos select 2) + 0.2];

_spawnTent setVariable ["position", _spawnPos, false];
private _spawnParts = _spawnTent getVariable "parts";

private _spawnMarker = createMarker [format["%1basehq", side_players],_spawnPos];
_spawnMarker setMarkerShapeLocal "ICON";
_spawnMarker setMarkerTypeLocal "b_hq";
_spawnMarker setMarkerTextLocal "Headquarters";
_spawnMarker setMarkerColorLocal "ColorBLUFOR";
_spawnTent setVariable ["spawnMarker", _spawnMarker, false];

_spawnTent setVariable ["respawn", ([side_players, _spawnPos, "Headquarters"] call BIS_fnc_addRespawnPosition)];
_spawnTent addEventHandler ["Deleted", {
	if (isServer) then {
		{
			deleteVehicle _x;
		} forEach ((_this select 0) getVariable "parts");
		((_this select 0) getVariable "respawn") call BIS_fnc_removeRespawnPosition;
		deleteMarker ((_this select 0) getVariable "spawnMarker");
	};
}];

[_spawnPos, _spawnDir, _spawnTent, (_spawnParts select 3), (_spawnParts select 4), _spawnMarker] spawn {
	params ["_spawnPos", "_spawnDir", "_spawnTent", "_spawnConTent", "_spawnHouse", "_spawnMarker"];

	_spawnTent setPosATL _spawnPos;
	_spawnTent setDir _spawnDir;
	private _spawnPosASL = getPosASL _spawnTent;

	private _connPos = ([0, 7, 0.25] call fnc_cartToPol);
	_connPos set [1, (_connPos select 1) + _spawnDir];
	_connPos set [3, _spawnPosASL];
	_connPos = _connPos call fnc_polToCart;

	private _housePos = ([-0.7, 9] call fnc_cartToPol);
	_housePos set [1, (_housePos select 1) + _spawnDir];
	_housePos set [3, _spawnPosASL];
	_housePos = _housePos call fnc_polToCart;

	_spawnConTent setPosASL _connPos;
	_spawnConTent setDir _spawnDir;
	_spawnHouse setPosASL _housePos;
	_spawnHouse setDir _spawnDir;

	_spawnMarker setMarkerPos [(_housePos select 0), (_housePos select 1), 3.5];
};

// Uncomment for arsenal at spawn.
//[_spawnBox, true] call ace_arsenal_fnc_initBox;
missionNamespace setVariable ["spawnPosition", _spawnPos, true];

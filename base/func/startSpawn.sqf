// Run at startup to move players into position.
// Usage: [] spawn fnc_startSpawn
// Returns: Nothing.
if (!isServer) exitWith {};
params ["_spawnPositions"];

private _spawn = _spawnPositions call fnc_getRandom;
private _spawnPos = (_spawn select 0);
private _vehicle = "B_Heli_Transport_03_unarmed_F" createVehicle _spawnPos;
_vehicle setDir (_spawn select 1);
[_vehicle] spawn {
	params ["_vehicle"];
	_vehicle setVariable ["respawn", ([side_players, _vehicle, "MCV Respawn"] call BIS_fnc_addRespawnPosition)];
	_vehicle addEventHandler ["Killed", {
		[] spawn {
			["mainObjective","FAILED"] call BIS_fnc_taskSetState;
			uisleep 3;
			["end4", true, true, true, true] remoteExec ["BIS_fnc_endMission", 0];
		};
	}];
	_vehicle addEventHandler ["Deleted", {
		if (isServer) then {
			private _vehicle = (_this select 0);
			(_vehicle getVariable "respawn") call BIS_fnc_removeRespawnPosition;
			[getPos _vehicle, getDir _vehicle, _vehicle getVariable "tent"] spawn fnc_base_deploySpawn;
		};
	}];
	[_vehicle] call fnc_ls_addToVehicle;

	[_vehicle, 0, ["ACE_MainActions"], ["spawnhelideploybase", "Deploy HQ", "",
		{
			deleteVehicle _target;
			[_player, "engineer", true] call fnc_base_becomeClass;
			[_player, format["Deployed HQ and became %1.", base_class_engineer select 1]] remoteExec ["sideChat", 0];
		}, { (((count (crew _target)) == 0) && (isNull ([_target, (zone_buffer * 2)] call fnc_inZone))) },
		{}, [], {[0, 0, 0]}, 2, [false, false, false, false, false], {}]]
		call fnc_addAceActionToObjectGlobal;
	[_vehicle, ["Deploy HQ",
		{
			deleteVehicle (_this select 0);
			[(_this select 1), "engineer", true] call fnc_base_becomeClass;
			[(_this select 1), format["Deployed HQ and became %1.", base_class_engineer select 1]] remoteExec ["sideChat", 0];
		},
		nil, 1.45, true, true, "",
		"(((count (crew _target)) == 0) && (isNull ([_target, (zone_buffer * 2)] call fnc_inZone)))", 6, false, "", ""]
	] call fnc_addActionGlobal;
};
for "_i" from 0 to count allCurators -1 do {
	(allCurators select _i) addCuratorEditableObjects [[_vehicle],true];
};
[_vehicle] spawn {
	params ["_vehicle"];
	private _spawnTent = "Land_MedicalTent_01_MTP_closed_F" createVehicle [-(random 500),-(random 500),(random 500) + 1000];
	_spawnTent allowDamage false;
	_spawnTent setObjectTextureGlobal [0,"\a3\Structures_F_Orange\Humanitarian\Camps\Data\MedicalTent_01_white_generic_F_CO.paa"];
	_spawnTent spawn {
		uisleep 2;
		_this animateSource ["Door_Hide",1,true];
	};
	private _spawnFloor = "Land_MedicalTent_01_floor_light_F" createVehicle [-(random 500),-(random 500),(random 500) + 1000];
	_spawnFloor attachTo [_spawnTent, [0,0,0]];
	_spawnFloor allowDamage false;

	private _spawnAirCon = "Land_AirConditioner_04_F" createVehicle [-(random 500),-(random 500),(random 500) + 1000];
	_spawnAirCon attachTo [_spawnTent, [-3.53,7.3,-0.86]];
	_spawnAirCon setDir 317;
	_spawnAirCon allowDamage false;

	private _spawnConTent = "Land_ConnectorTent_01_white_open_F" createVehicle [-(random 500),-(random 500),(random 500) + 1000];
	_spawnConTent allowDamage false;

	private _spawnHouse = "Land_Medevac_house_V1_F" createVehicle [-(random 500),-(random 500),(random 500) + 1000];
	_spawnHouse allowDamage false;

	private _spawnLaptop = "Land_Laptop_unfolded_F" createVehicle [-(random 500),-(random 500),(random 500) + 1000];
	_spawnLaptop allowDamage false;
	_spawnLaptop attachTo [_spawnTent, [-2.5,-1.4,-0.45]];
	_spawnLaptop setDir 125.5;

	_spawnTent setVariable ["parts", [_spawnTent, _spawnFloor, _spawnAirCon, _spawnConTent, _spawnHouse, _spawnLaptop], false];

	[_spawnTent] spawn {
		params ["_spawnTent"];
		private _stretcherOne = "Land_Stretcher_01_F" createVehicle [-(random 500),-(random 500),(random 500) + 1000];
		_stretcherOne attachTo [_spawnTent, [2.3, -3, -1.2]];
		_stretcherOne setDir 90;
		private _stretcherTwo = "Land_Stretcher_01_F" createVehicle [-(random 500),-(random 500),(random 500) + 1000];
		_stretcherTwo attachTo [_spawnTent, [2.3, 0, -1.2]];
		_stretcherTwo setDir 90;
		private _stretcherThree = "Land_Stretcher_01_F" createVehicle [-(random 500),-(random 500),(random 500) + 1000];
		_stretcherThree attachTo [_spawnTent, [2.3, 3, -1.2]];
		_stretcherThree setDir 90;
		private _tableOne = "Land_CampingTable_White_F" createVehicle [-(random 500),-(random 500),(random 500) + 1000];
		_tableOne attachTo [_spawnTent, [-2.5, -2, -1]];
		_tableOne setDir 270;
		_tableOne setVariable ["ace_dragging_canCarry", false, true];
		private _tableTwo = "Land_CampingTable_White_F" createVehicle [-(random 500),-(random 500),(random 500) + 1000];
		_tableTwo attachTo [_spawnTent, [-2.5, 2, -1]];
		_tableTwo setDir 270;
		_tableTwo setVariable ["ace_dragging_canCarry", false, true];
		_spawnTent setVariable ["decorations", [_stretcherOne, _stretcherTwo, _stretcherThree, _tableOne, _tableTwo], false];
	};

	[_spawnHouse] spawn {
		params ["_spawnHouse"];
		private _spawnDesk = "Land_PortableDesk_01_black_F" createVehicle [-(random 500),-(random 500),(random 500) + 1000];
		private _laptopTwo = "Land_Laptop_unfolded_F" createVehicle [-(random 500),-(random 500),(random 500) + 1000];
		clearItemCargoGlobal _spawnDesk;
		clearMagazineCargoGlobal _spawnDesk;
		clearWeaponCargoGlobal _spawnDesk;
		clearBackpackCargoGlobal _spawnDesk;
		_spawnDesk allowDamage false;
		_spawnDesk animateSource ["Wing_R_Hide_Source", 1, true];
		_laptopTwo allowDamage false;
		_laptopTwo attachTo [_spawnDesk, [-0.498,0.064,0.6]];
		_laptopTwo setDir 160;
		_spawnDesk attachTo [_spawnHouse, [-2.206, 1.271, 0.32]];
		_spawnDesk setDir 270;
		_laptopTwo setObjectTextureGlobal [0, "textures\bscreen.paa"]; // #(argb,8,8,3)color(0,0,0.6,1.0,co)

		private _serverOne = "Land_PortableServer_01_black_F" createVehicle [-(random 500),-(random 500),(random 500) + 1000];
		_serverOne attachTo [_spawnHouse, [-2.2, 3.5, 0.205]];// 0.346
		private _serverTwo = "Land_PortableServer_01_black_F" createVehicle [-(random 500),-(random 500),(random 500) + 1000];
		_serverTwo attachTo [_spawnHouse, [-2.2, 3.5, 0.551]]; // 0.346
		private _serverThree = "Land_PortableServer_01_black_F" createVehicle [-(random 500),-(random 500),(random 500) + 1000];
		_serverThree attachTo [_spawnHouse, [-2.2, 3.5, 0.897]]; // 0.35
		private _serverFour = "Land_Router_01_black_F" createVehicle [-(random 500),-(random 500),(random 500) + 1000];
		_serverFour attachTo [_spawnHouse, [-2.07, 3.248, 1.17]];
		_spawnHouse setVariable ["decorations", [_serverOne, _serverTwo, _serverThree, _serverFour, _spawnDesk, _laptopTwo], false];
	};

	[_spawnLaptop] spawn {
		params ["_spawnLaptop"];

		if (ace_available) then {
			[_spawnLaptop, -1] call ace_cargo_fnc_setSize;
			[_spawnLaptop, 0, ["ACE_MainActions"], base_action_buy]
				call fnc_addAceActionToObjectGlobal;
			// Respawns
			[_spawnLaptop, 0, ["ACE_MainActions", "action_buy"], base_action_buy_respawns_ten]
				call fnc_addAceActionToObjectGlobal;
			[_spawnLaptop, 0, ["ACE_MainActions", "action_buy", "action_buy_respawns_ten"], base_action_buy_respawns_cent]
				call fnc_addAceActionToObjectGlobal;
			[_spawnLaptop, 0, ["ACE_MainActions", "action_buy", "action_buy_respawns_ten"], base_action_buy_respawns_mil]
				call fnc_addAceActionToObjectGlobal;
			// Class Upgrades (Just Engineer)
			[_spawnLaptop, 0, ["ACE_MainActions", "action_buy"], base_action_buy_class]
				call fnc_addAceActionToObjectGlobal;
			[_spawnLaptop, 0, ["ACE_MainActions", "action_buy", "action_buy_class"], base_aceaction_buy_class_engineer]
				call fnc_addAceActionToObjectGlobal;
		};
		[_spawnLaptop, base_action_buy_class_engineer] call fnc_addActionGlobal;

		[_spawnLaptop, [format["Buy %1 Respawns, ($%2)", 10, 1000], {
				params ["_target", "_caller", "_actionId", "_arguments"];
				[side _caller, 10, _caller] remoteExec ["fnc_base_buyrespawns", 2];
			},
			nil, 1.45, true, true, "",
			"(missionNamespace getVariable ['base_cash', 0]) >= 1000", 2, false, "", ""]] call fnc_addActionGlobal;
	};

	_vehicle setVariable ["tent", _spawnTent, true];
};
missionNamespace setVariable ["spawnPosition", _spawnPos, true];

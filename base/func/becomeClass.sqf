// Classy
params ["_unit", "_class", ["_once", false]];
if (missionNamespace getVariable ["base_engineer", objNull] == _unit) then {
	missionNamespace setVariable ["base_engineer", objNull, true];
	[_unit,1,["ACE_SelfActions", "action_engineer_build", "action_engineer_build_barracks"]] call ace_interact_menu_fnc_removeActionFromObject;
	[_unit,1,["ACE_SelfActions", "action_engineer_build", "action_engineer_build_depot"]] call ace_interact_menu_fnc_removeActionFromObject;
	[_unit,1,["ACE_SelfActions", "action_engineer_build", "action_engineer_build_ifvturret"]] call ace_interact_menu_fnc_removeActionFromObject;
	[_unit,1,["ACE_SelfActions", "action_engineer_build", "action_engineer_build_mrkturret"]] call ace_interact_menu_fnc_removeActionFromObject;
	[_unit,1,["ACE_SelfActions", "action_engineer_build"]] call ace_interact_menu_fnc_removeActionFromObject;
	[_unit,1,["ACE_SelfActions", "action_engineer_build_medical"]] call ace_interact_menu_fnc_removeActionFromObject;
};
_unit setVariable ["class", _class, true];
switch (_class) do {
	case "engineer": {
		missionNamespace setVariable ["base_engineer", _unit, true];
		_unit setUnitTrait ["engineer",true];
		_unit setVariable ["ACE_IsEngineer",2,true];
		if (_once) then {
			[_unit, 1, ["ACE_SelfActions"], base_action_engineer_build_medical]
				call fnc_addAceActionToObjectGlobal;
			[_unit, 1, ["ACE_SelfActions"], base_action_engineer_build]
				call fnc_addAceActionToObjectGlobal;
			[_unit, 1, ["ACE_SelfActions", "action_engineer_build"], base_action_engineer_build_depot]
				call fnc_addAceActionToObjectGlobal;
			[_unit, 1, ["ACE_SelfActions", "action_engineer_build"], base_action_engineer_build_barracks]
				call fnc_addAceActionToObjectGlobal;
			[_unit, 1, ["ACE_SelfActions", "action_engineer_build"], base_action_engineer_build_ifvturret]
				call fnc_addAceActionToObjectGlobal;
			[_unit, 1, ["ACE_SelfActions", "action_engineer_build"], base_action_engineer_build_mrkturret]
				call fnc_addAceActionToObjectGlobal;
		};
	};
	case "eod": {
		_unit setUnitTrait ["engineer",true];
		_unit setUnitTrait ["explosiveSpecialist",true];
		_unit setVariable ["ACE_IsEngineer",2,true];
	};
	case "medic": {
		_unit setUnitTrait ["medic",true];
	};
	case "jtac": {
		_unit setUnitTrait ["uavhacker",true];
	};
};
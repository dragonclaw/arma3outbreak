// Stuff
if (!isServer) exitWith {};
params ["_target", "_position"];
if (!isNull (attachedTo _target)) then {
	_target = (attachedTo _target);
};
[_target, false] remoteExec ["enableSimulationGlobal", 2];
if (isDamageAllowed _target) then {
	_target setVariable ["boxfrozen", true, true];
	[_target, false] remoteExec ["allowDamage", 2];
};
private _box = (_target getVariable ["boxtype", "Land_PlasticCase_01_large_black_F"]) createVehicle [_position select 0, _position select 1, (_position select 2)];
_box allowDamage false;
_target attachTo [_box, [(random 1000) - 500, (random 1000) - 500, (random 1000) + 22000]];
clearItemCargoGlobal _box;
clearMagazineCargoGlobal _box;
clearWeaponCargoGlobal _box;
clearBackpackCargoGlobal _box;
private _unbox_action = base_action_unbox;
private _name = _target getVariable ["boxname", "_"];
if (_name != "_") then {
	_unbox_action = +base_action_unbox;
	_unbox_action set [1, format["Deploy %1", _name]];
	_box setVariable ["ace_cargo_customname", _name, true];
};
private _class = _target getVariable ["boxclass", "_"];
private _condition = 'isNull (attachedTo _target) && isNull (_this getVariable ["boxcarry", objNull]) && (isNull ([_target, (zone_buffer * 2)] call fnc_inZone))';
if (_class != "_") then {
	_condition = format['isNull (attachedTo _target) && (isNull ([_target, (zone_buffer * 2)] call fnc_inZone)) && (_this getVariable ["class", "_"]) == "%1"', _class];
	_unbox_action set [4, compile format['isNull (attachedTo _target) && (isNull ([_target, (zone_buffer * 2)] call fnc_inZone)) && (_player getVariable ["class", "_"]) == "%1"', _class]];
};
[_box, _unbox_action, _condition] spawn {
	params ["_box", "_unbox_action", "_condition"];
	_unbox_action params ["_id", "_name", "_short", "_action", "_compiledCondition", "_children", "_params", "_position", "_distance", "_aceparams", "_modifier"];
	[_box, [_name, { deleteVehicle (_this select 0); }, nil, 1.6, true, true, "", _condition, _distance, false, "", ""]]
		call fnc_addActionGlobal;
	[_box, [format["Pick up %1", (_box getVariable ["ace_cargo_customname", "Box"])],
	{
		(_this select 1) setVariable ["boxcarry", (_this select 0)];
		(_this select 0) attachTo [(_this select 1), [0,-0.6,0], "spine3"];
		(_this select 1) addAction [format["Drop %1", ((_this select 0) getVariable ["ace_cargo_customname", "Box"])],
			{
				(_this select 1) removeAction (_this select 2);
				detach ((_this select 1) getVariable "boxcarry");
				(_this select 1) setVariable ["boxcarry", objNull];
			},nil, 1.5, true, true, "", "true", -1, false, "", ""];
		(_this select 0) setDir 270;
	}, nil, 1.5, true, true, "", _condition, _distance, false, "", ""]]
		call fnc_addActionGlobal;
	if (ace_available) then {
		[_box, 0, ["ACE_MainActions"], _unbox_action]
			call fnc_addAceActionToObjectGlobal;
	};
};
for "_i" from 0 to count allCurators -1 do { 
	(allCurators select _i) addCuratorEditableObjects [[_box],TRUE]; 
};
[_box, true, [0, 2, 1], 270, true] remoteExec ["ace_dragging_fnc_setCarryable", 0];
[_box, -1] remoteExec ["ace_cargo_fnc_setSpace", 0];
_box addEventHandler ["Deleted", {
	if (isServer) then {
		params ["_box"];
		private _objects = attachedObjects _box;
		private _dir = getDir _box;
		private _pos = getPos _box;
		_box setPos [-4500, -4500, -4000];
		for "_i" from 0 to count _objects -1 do {
			[(_objects select _i), _dir, _pos] spawn {
				params["_object", "_dir", "_pos"];
				detach _object;
				_object setDir _dir;
				_object setPos _pos;
				[_object, true] remoteExec ["enableSimulationGlobal", 2];
				if (_object getVariable ["boxfrozen", false]) then {
					uisleep 0.2;
					_object setDamage 0.1;
					_object setDamage 0;
					uisleep 2;
					[_object, true] remoteExec ["allowDamage", 2];
					_object setDamage 0;
				};
			};
		};
	};
}];
_box
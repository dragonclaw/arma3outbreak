if (!isServer) exitWith {};
params["_target", "_player"];
private _zone = ([getPos _target] call fnc_inZone);
private _dir = getDir _target;
private _pos = getPos _target;
if (isNull _zone) exitWith {};
if (!isNull (_zone getVariable ["medical", objNull])) exitWith {};
deleteVehicle _target;
private _medicalTent = "Land_MedicalTent_01_MTP_closed_F" createVehicle [-(random 500),-(random 500),(random 500) + 1000];
_medicalTent allowDamage false;
_medicalTent setDir _dir;
_medicalTent setPos _pos;
_medicalTent setObjectTextureGlobal [0,"\a3\Structures_F_Orange\Humanitarian\Camps\Data\MedicalTent_01_white_generic_F_CO.paa"];
private _medicalFloor = "Land_MedicalTent_01_floor_light_F" createVehicle [-(random 500),-(random 500),(random 500) + 1000];
_medicalFloor attachTo [_medicalTent, [0,0,0]];
_medicalTent setVariable ["floor", _medicalFloor];
_medicalTent setVariable ["respawn", ([side_players, _pos, format["%1 Medical Centre", (_zone getVariable["name", "Someplace"])]] call BIS_fnc_addRespawnPosition)];
[_medicalTent] spawn {
	params ["_medicalTent"];
	private _stretcherOne = "Land_Stretcher_01_F" createVehicle [-(random 500),-(random 500),(random 500) + 1000];
	_stretcherOne attachTo [_medicalTent, [2.3, -3, -1.2]];
	_stretcherOne setDir 90;
	private _stretcherTwo = "Land_Stretcher_01_F" createVehicle [-(random 500),-(random 500),(random 500) + 1000];
	_stretcherTwo attachTo [_medicalTent, [2.3, 0, -1.2]];
	_stretcherTwo setDir 90;
	private _stretcherThree = "Land_Stretcher_01_F" createVehicle [-(random 500),-(random 500),(random 500) + 1000];
	_stretcherThree attachTo [_medicalTent, [2.3, 3, -1.2]];
	_stretcherThree setDir 90;
	private _tableOne = "Land_CampingTable_White_F" createVehicle [-(random 500),-(random 500),(random 500) + 1000];
	_tableOne attachTo [_medicalTent, [-2.5, -2, -1]];
	_tableOne setDir 270;
	_tableOne setVariable ["ace_dragging_canCarry", false, true];
	private _tableTwo = "Land_CampingTable_White_F" createVehicle [-(random 500),-(random 500),(random 500) + 1000];
	_tableTwo attachTo [_medicalTent, [-2.5, 2, -1]];
	_tableTwo setDir 270;
	_tableTwo setVariable ["ace_dragging_canCarry", false, true];
	_medicalTent setVariable ["decorations", [_stretcherOne, _stretcherTwo, _stretcherThree, _tableOne, _tableTwo], false];
};
_medicalTent addEventHandler ["Deleted", {
	if (isServer) then {
		((_this select 0) getVariable "respawn") call BIS_fnc_removeRespawnPosition;
		deleteVehicle ((_this select 0) getVariable "floor");
		{
			deleteVehicle _x;
		} forEach ((_this select 0) getVariable ["decorations", []]);
	};
}];
_zone setVariable ["medical", _medicalTent, true];
_medicalTent spawn {
	uisleep 2;
	_this animateSource ["Door_Hide",1,true];
};
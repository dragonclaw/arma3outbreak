private _vehicleactionScript = [] execVM "base\vehicle_actions.sqf";
waitUntil { scriptDone _vehicleactionScript };

base_action_buy = ["action_buy", "Buy", "", {},
	{ [_player, _target, []] call ace_common_fnc_canInteractWith },
	{}, [], {[0, 0, 0]}, 2, [false, false, false, false, false], {}];

// Respawns
base_action_buy_respawns_ten = ["action_buy_respawns_ten", "Respawns (+10) ($1,000)", "",
	{ [side _player, 10, _player] remoteExec ["fnc_base_buyrespawns", 2]; },
	{ (missionNamespace getVariable ["base_cash", 0]) >= 1000 },
	{}, [], {[0, 0, 0]}, 2, [false, false, false, false, false], {}];
base_action_buy_respawns_cent = ["action_buy_respawns_cent", "Respawns (+100) ($10,000)", "",
	{ [side _player, 100, _player] remoteExec ["fnc_base_buyrespawns", 2]; },
	{ (missionNamespace getVariable ["base_cash", 0]) >= 10000 },
	{}, [], {[0, 0, 0]}, 2, [false, false, false, false, false], {}];
base_action_buy_respawns_mil = ["action_buy_respawns_mil", "Respawns (+1,000) ($100,000)", "",
	{ [side _player, 1000, _player] remoteExec ["fnc_base_buyrespawns", 2]; },
	{ (missionNamespace getVariable ["base_cash", 0]) >= 100000 },
	{}, [], {[0, 0, 0]}, 2, [false, false, false, false, false], {}];

// Logistics
base_action_buy_logistics = ["action_buy_logistics", "Logistics", "", {},
	{ [_player, _target, []] call ace_common_fnc_canInteractWith },
	{}, [], {[0, 0, 0]}, 2, [false, false, false, false, false], {}];
base_actions_buy_logistics = [
	["ACE_Wheel", "Spare Wheel", 25],
	["ACE_Track", "Spare Track", 100],
	["Land_CanisterFuel_F", "Jerry Can", 150],
	["B_HMG_01_A_F", "Auto HMG", 200, "jtac"],
	["B_GMG_01_A_F", "Auto GMG", 200, "jtac"]];

// Classes
base_class_engineer = ["engineer", localize "STR_B_engineer_F0", 0];
base_class_eod = ["eod", localize "STR_B_soldier_repair_F0", 200];
base_class_medic = ["medic", localize "STR_B_medic_F0", 400];
base_class_jtac = ["jtac", localize "STR_A3_B_SOLDIER_UAV_F0", 400];
base_action_buy_class = ["action_buy_class", "Class", "", {},
	{ [_player, _target, []] call ace_common_fnc_canInteractWith },
	{}, [], {[0, 0, 0]}, 2, [false, false, false, false, false], {}];
base_aceaction_buy_class_engineer = ["action_buy_class_engineer", "Engineer ($0)", "",
	{
		[_player, format["%1 chosen.", base_class_engineer select 1]] remoteExec ["sideChat", 0];
		[_player, "engineer", true] call fnc_base_becomeClass;
	},
	{ (_player getVariable ["class", "_"]) == "_" && isNull (missionNamespace getVariable ["base_engineer", objNull]) },
	{}, [], {[0, 0, 0]}, 2, [false, false, false, false, false], {}];
base_action_buy_class_engineer = [format["Buy Class: %1, ($%2)", base_class_engineer select 1, base_class_engineer select 2],
	{
		[_this select 1, format["%1 chosen.", base_class_engineer select 1]] remoteExec ["sideChat", 0];
		[_this select 1, base_class_engineer select 0, true] call fnc_base_becomeClass;
	},
	nil, 1.45, true, true, "",
	"(_this getVariable [""class"", ""_""]) == ""_"" && isNull (missionNamespace getVariable [""base_engineer"", objNull])", 2, false, "", ""];

base_action_engineer_build = ["action_engineer_build", "Construct", "", { },
	{ (_player getVariable ["class", "_"]) == "engineer" && (isNull ([_player, (zone_buffer * 2)] call fnc_inZone)) },
	{}, [], {[0, 0, 0]}, 2, [false, false, false, false, false], {}];
base_action_engineer_build_depot = ["action_engineer_build_depot", "Vehicle Depot ($800)", "", {
		if ([800, _player, "Vehicle Depot"] call fnc_base_spendcash) then {
			[_target, _player] remoteExec ["fnc_spawn_depot", 2];
		};
	},
	{ (missionNamespace getVariable ["base_cash", 0]) >= 800 },
	{}, [], {[0, 0, 0]}, 2, [false, false, false, false, false], {}];
base_action_engineer_build_barracks = ["action_engineer_build_barracks", "Barracks ($400)", "", {
		if ([400, _player, "Barracks"] call fnc_base_spendcash) then {
			[_target, _player] remoteExec ["fnc_spawn_barracks", 2];
		};
	},
	{ (missionNamespace getVariable ["base_cash", 0]) >= 400 },
	{}, [], {[0, 0, 0]}, 2, [false, false, false, false, false], {}];
base_action_engineer_build_medical = ["action_engineer_build_medical", "Build Medical Tent ($2,000)", "", {
		if ([2000, _player, "Medical Tent"] call fnc_base_spendcash) then {
			[_target, _player] remoteExec ["fnc_spawn_medical", 2];
		};
	},
	{ !(isNull ([getPos _player] call fnc_inZone)) && isNull (([getPos _target] call fnc_inZone) getVariable ['medical', objNull]) && (missionNamespace getVariable ["base_cash", 0]) >= 2000 },
	{}, [], {[0, 0, 0]}, 2, [false, false, false, false, false], {}];
base_action_engineer_build_ifvturret = ["action_engineer_build_ifvturret", "Static 20mm Cannon ($3,000)", "", {
		if ([3000, _player, "20mm Turret"] call fnc_base_spendcash) then {
			[_target, _player] remoteExec ["fnc_spawn_ifvturret", 2];
		};
	},
	{ (missionNamespace getVariable ["base_cash", 0]) >= 3000 },
	{}, [], {[0, 0, 0]}, 2, [false, false, false, false, false], {}];
base_action_engineer_build_mrkturret = ["action_engineer_build_mrkturret", "Static 120mm Cannon ($3,000)", "", {
		if ([3000, _player, "120mm Turret"] call fnc_base_spendcash) then {
			[_target, _player] remoteExec ["fnc_spawn_mrkturret", 2];
		};
	},
	{ (missionNamespace getVariable ["base_cash", 0]) >= 3000 },
	{}, [], {[0, 0, 0]}, 2, [false, false, false, false, false], {}];

base_action_unbox = ["action_unbox_container", "Deploy Here", "", {
		deleteVehicle _target;
	},
	{ isNull (attachedTo _target) && (isNull ([_target, (zone_buffer * 2)] call fnc_inZone)) }, {}, [], {[0, 0, 0]}, 4, [false, false, false, false, false], {}];

base_action_rebox = ["action_rebox_container", "Pack Up", "", {
		[_target] remoteExec ["fnc_rebox", 2];
	},
	{ (_target getVariable ["boxclass", "_"]) == "_" || (_player getVariable ["class", "_"]) == (_target getVariable ["boxclass", "_"]) }, {}, [], {[0, 0, 0]}, 2, [false, false, false, false, false], {}];
fnc_rebox = {
	if (!isServer) exitWith {};
	params ["_target"];
	private _dir = getDir _target;
	private _pos = getPos _target;
	private _box = [_target, _pos] call fnc_base_containerize;
	_box setDir _dir;
	_box setPos _pos;
};

fnc_spawn_depot = {
	if (!isServer) exitWith {};
	params["_target", "_player"];
	private _depot = "Land_RepairDepot_01_green_F" createVehicle [-5000, -5000, 10000];
	_depot allowDamage false;
	_depot setVariable ["boxname", "Vehicle Depot", true];
	_depot setVariable ["boxclass", "engineer", true];
	_depot setVariable ["boxtype", "B_Slingload_01_Cargo_F", true];
	_depot setVariable ["boxsize", 8, true];
	private _toped = "Land_JumpTarget_F" createVehicle [-5010, -5000, 10000];
	_toped attachTo [_depot, [-10, 0, 3]];
	_toped setVariable ["ACE_isRepairFacility", 1, true];
	_depot setVariable ["repairspot", _toped];
	private _laptop1 = "Land_Laptop_unfolded_F" createVehicle [-5000, -5000, 10000];
	_laptop1 allowDamage false;
	_laptop1 attachTo [_depot, [-0.45,0.5,-0.69]];
	_laptop1 setDir 238;
	_laptop1 setVariable ["depot", _depot];
	_laptop1 setVariable ["repairspot", _toped];
	private _laptop2 = "Land_Laptop_unfolded_F" createVehicle [-5000, -5000, 10000];
	_laptop2 allowDamage false;
	_laptop2 attachTo [_depot, [1,-0.45,-0.69]];
	_laptop2 setDir 58;
	_laptop2 setVariable ["depot", _depot];
	_laptop2 setVariable ["repairspot", _toped];
	private _box = [_depot, getPos _player] call fnc_base_containerize;
	clearItemCargoGlobal _depot;
	clearMagazineCargoGlobal _depot;
	clearWeaponCargoGlobal _depot;
	clearBackpackCargoGlobal _depot;

	[_depot, 1000000] call ace_cargo_fnc_setSpace;
	[_depot, 24000, [[-0.7,-2.3,-1.3]]] call ace_refuel_fnc_makeSource;
	_depot setVariable ["ace_refuel_hoseLength", 10, true];
	_depot setVariable ["ace_rearm_isSupplyVehicle", true, true];
	_depot setVariable ["rearm", ([_depot] remoteExec ["ace_rearm_fnc_initSupplyVehicle", 0, true])];

	[_depot, 0, ["ACE_MainActions"], base_action_rebox]
		call fnc_addAceActionToObjectGlobal;
	{
		private _laptop = _x;
		[_laptop, 0, ["ACE_MainActions"], base_action_buy]
			call fnc_addAceActionToObjectGlobal;

		// Logistics
		[_laptop, 0, ["ACE_MainActions", "action_buy"], base_action_buy_logistics]
			call fnc_addAceActionToObjectGlobal;

		{
			private _logtype = _x select 0;
			private _logname = _x select 1;
			private _logprice = _x select 2;
			private _condition = (compile format["(missionNamespace getVariable ['base_cash', 0]) >= %1", _logprice]);
			if ((count _x) > 3) then {
				private _class = _x select 3;
				_condition = (compile format["(missionNamespace getVariable ['base_cash', 0]) >= %1 && (_player getVariable ['class', '_']) == 'jtac'", _logprice, _class]);
			};
			private _logbuyname = format["action_buy_logistics_%1", _logtype];
			[_laptop, 0, ["ACE_MainActions", "action_buy", "action_buy_logistics"],
				[_logbuyname, format ["Buy %1 ($%2)", _logname, _logprice], "", {
					(_this select 2) params ["_logtype", "_logname", "_logprice"];
					if ([_logprice, _player, _logname] call fnc_base_spendcash) then {
						[_logtype, (_target getVariable "depot"), true] call ace_cargo_fnc_loadItem;
					};
				},
				_condition,
				{}, [_logtype, _logname, _logprice], {[0, 0, 0]}, 2, [false, false, false, false, false], {}]
			] call fnc_addAceActionToObjectGlobal;
		} forEach base_actions_buy_logistics;

		// Vehicles
		[_laptop, 0, ["ACE_MainActions", "action_buy"], base_action_buy_vehicle]
			call fnc_addAceActionToObjectGlobal;
		[_laptop, 0, ["ACE_MainActions", "action_buy"], base_action_buy_helicopter]
			call fnc_addAceActionToObjectGlobal;
		[_laptop, 0, ["ACE_MainActions", "action_buy"], base_action_buy_tank]
			call fnc_addAceActionToObjectGlobal;
		[_laptop, 0, ["ACE_MainActions", "action_buy"], base_action_buy_drone]
			call fnc_addAceActionToObjectGlobal;
		
		{
			private _catagory = _x select 0;
			private _vehtypes = _x select 1;
			{
				private _vehtype = _x select 0;
				private _vehname = _x select 1;
				private _vehprice = _x select 2;
				private _subtypes = _x select 3;
				private _vehbuyname = format["action_buy_vehicle_%1", _vehtype];
				[_laptop, 0, ["ACE_MainActions", "action_buy", _catagory],
					[_vehbuyname, format ["Buy %1 ($%2)", _vehname, _vehprice], "", {
						(_this select 2) params ["_vehtype", "_vehname", "_vehprice"];
						if ([_vehprice, _player, _vehname] call fnc_base_spendcash) then {
							[_target, _player] remoteExec [(format["fnc_spawn_%1", _vehtype]), 2];
						};
					},
					(compile format["(missionNamespace getVariable [""base_cash"", 0]) >= %1", _vehprice]),
					{}, [_vehtype, _vehname, _vehprice], {[0, 0, 0]}, 2, [false, false, false, false, false], {}]
				] call fnc_addAceActionToObjectGlobal;
				{
					private _subtype = _x select 0;
					private _subname = _x select 1;
					private _subprice = _x select 2;
					[_laptop, 0, ["ACE_MainActions", "action_buy", _catagory, _vehbuyname],
						[format ["action_buy_vehicle_%1", _subtype], format ["Buy %1 ($%2)", _subname, _subprice], "", {
							(_this select 2) params ["_subtype", "_subname", "_subprice"];
							if ([_subprice, _player, _subname] call fnc_base_spendcash) then {
								[_target, _player] remoteExec [(format["fnc_spawn_%1", _subtype]), 2];
							};
						},
						(compile format["(missionNamespace getVariable [""base_cash"", 0]) >= %1", _subprice]),
						{}, [_subtype, _subname, _subprice], {[0, 0, 0]}, 2, [false, false, false, false, false], {}]
					] call fnc_addAceActionToObjectGlobal;
				} forEach _subtypes;
			} forEach _vehtypes;
		} forEach base_actions_buy_vehicles;
	} forEach [_laptop1, _laptop2];
	_box
};

fnc_spawn_barracks = {
	if (!isServer) exitWith {};
	params["_target", "_player"];
	private _barracks = "Land_MedicalTent_01_NATO_generic_outer_F" createVehicle [-5000, -5000, 10000];
	_barracks setObjectTextureGlobal [0,"\a3\Structures_F_Orange\Humanitarian\Camps\Data\MedicalTent_01_white_generic_F_CO.paa"];
	_barracks allowDamage false;
	_barracks setVariable ["boxname", "Barracks", true];
	private _table = "Land_CampingTable_white_F" createVehicle [-5000, -5000, 10000];
	_table allowDamage false;
	_table attachTo [_barracks, [3.25,0,-0.91]];
	_table setDir 90;
	_table setVariable ["ace_dragging_canCarry", false, true];
	private _laptop = "Land_Laptop_unfolded_F" createVehicle [-5000, -5000, 10000];
	_laptop allowDamage false;
	_laptop attachTo [_barracks, [3.192,0.546,-0.35]];
	_laptop setDir 248;
	private _chair = "Land_CampingChair_V1_F" createVehicle [-5000, -5000, 10000];
	_chair allowDamage false;
	_chair attachTo [_barracks, [2,-0.8,-0.81]];
	_chair setDir 248;
	_chair setVariable ["ace_dragging_canCarry", false, true];
	private _box1 = "Land_Pallet_MilBoxes_F" createVehicle [-5000, -5000, 10000];
	_box1 allowDamage false;
	_box1 attachTo [_barracks, [-3,0,-0.9]];
	private _box2 = "Land_PaperBox_open_full_F" createVehicle [-5000, -5000, 10000];
	_box2 allowDamage false;
	_box2 attachTo [_barracks, [-3,2,-0.78]];
	private _box3 = "Land_PaperBox_open_full_F" createVehicle [-5000, -5000, 10000];
	_box3 allowDamage false;
	_box3 attachTo [_barracks, [-3,-2,-0.78]];
	private _box4 = "Land_Pallet_MilBoxes_F" createVehicle [-5000, -5000, 10000];
	_box4 allowDamage false;
	_box4 attachTo [_barracks, [-3,4,-0.9]];
	_box4 setDir 180;
	private _box5 = "Land_Pallet_MilBoxes_F" createVehicle [-5000, -5000, 10000];
	_box5 allowDamage false;
	_box5 attachTo [_barracks, [-3,-4,-0.9]];
	_box5 setDir 270;
	private _floor = "Land_MedicalTent_01_floor_light_F" createVehicle [-(random 500),-(random 500),(random 500) + 1000];
	_floor attachTo [_barracks, [0,0,0]];
	_barracks setVariable ["laptop", _laptop, true];
	_barracks setVariable ["table", _table];
	_barracks setVariable ["chair", _chair];
	_barracks setVariable ["boxes", [_box1, _box2, _box3, _box4, _box5]];
	_barracks setVariable ["floor", _floor];
	_barracks setVariable ["boxclass", "engineer", true];
	private _box = [_barracks, getPos _player] call fnc_base_containerize;
	clearItemCargoGlobal _barracks;
	clearMagazineCargoGlobal _barracks;
	clearWeaponCargoGlobal _barracks;
	clearBackpackCargoGlobal _barracks;
	[_barracks, 0, ["ACE_MainActions"], base_action_rebox]
		call fnc_addAceActionToObjectGlobal;

	[_barracks, _laptop] spawn {
		params ["_barracks", "_laptop"];
		private _classes = [base_class_eod, base_class_medic, base_class_jtac];
		private _action = { ["Open",true] call BIS_fnc_arsenal; };
		if (ace_available) then {
			[_laptop, 0, ["ACE_MainActions"], base_action_buy]
				call fnc_addAceActionToObjectGlobal;
			[_laptop, 0, ["ACE_MainActions", "action_buy"], base_action_buy_class]
				call fnc_addAceActionToObjectGlobal;
			{
				_x params ["_classid", "_classname", "_classcost"];
				[_laptop, 0, ["ACE_MainActions", "action_buy", "action_buy_class"], 
					[format["action_buy_class_%1", _x select 0], format["%1 ($%2)", _classname, _classcost], "",
						(compile format["if ([%3, _player, ""Class: %2""] call fnc_base_spendcash) then { [_player, ""%1"", true] call fnc_base_becomeClass; };", _classid, _classname, _classcost]),
						(compile format["(_player getVariable [""class"", ""_""]) == ""_"" && (missionNamespace getVariable [""base_cash"", 0]) >= %1", _classcost]),
						{}, [], {[0, 0, 0]}, 2, [false, false, false, false, false], {}
					]
				] call fnc_addAceActionToObjectGlobal;
			} forEach _classes;

			[_laptop, 0, ["ACE_MainActions", "action_buy", "action_buy_class"], base_aceaction_buy_class_engineer]
				call fnc_addAceActionToObjectGlobal;
			[_laptop, base_action_buy_class_engineer] call fnc_addActionGlobal;

			[_laptop, ["Unlearn Class", { [_this select 1, "_", true] call fnc_base_becomeClass; },
					nil, 1.3, true, true, "",
					"(_this getVariable [""class"", ""_""]) != ""_""", 2, false, "", ""]
				] call fnc_addActionGlobal;

			[_laptop, true] call ace_arsenal_fnc_initBox;
			_action = {
				params ["_target", "_caller", "_actionId", "_arguments"];
				[(_target getVariable "laptop"), _caller] call ace_arsenal_fnc_openBox;
			};
		};
		{
			_x params ["_classid", "_classname", "_classcost"];
			[_laptop, 0, ["ACE_MainActions", "action_buy", "action_buy_class"], 
				[format["action_buy_class_%1", _x select 0], format["%1 ($%2)", _classname, _classcost], "",
					(compile format["if ([%3, _player, ""Class: %2""] call fnc_base_spendcash) then { [_player, ""%1"", true] call fnc_base_becomeClass; };", _classid, _classname, _classcost]),
					(compile format["(_player getVariable [""class"", ""_""]) == ""_"" && (missionNamespace getVariable [""base_cash"", 0]) >= %1", _classcost]),
					{}, [], {[0, 0, 0]}, 2, [false, false, false, false, false], {}
				]
			] call fnc_addAceActionToObjectGlobal;

			[_laptop, [format["Buy Class: %1 ($%2)", _classname, _classcost], 
				(compile format["if ([%3, _this select 1, ""Class: %2""] call fnc_base_spendcash) then { [_this select 1, ""%1"", true] call fnc_base_becomeClass; };", _classid, _classname, _classcost]),
				nil, 1.4, true, true, "",
				(format["(_this getVariable [""class"", ""_""]) == ""_"" && (missionNamespace getVariable [""base_cash"", 0]) >= %1", _classcost]), 2, false, "", ""]
			] call fnc_addActionGlobal;
		} forEach _classes;

		[_barracks, [localize "STR_A3_Arsenal", _action, nil, 1.5, true, true, "", "true", 6, false, "", ""]]
			call fnc_addActionGlobal;
	};

	_box
};

fnc_spawn_medical = {
	if (!isServer) exitWith {};
	params["_target", "_player"];
	private _box = "Box_IDAP_Equip_F" createVehicle (getPos _player);
	clearItemCargoGlobal _box;
	clearMagazineCargoGlobal _box;
	clearWeaponCargoGlobal _box;
	clearBackpackCargoGlobal _box;
	[_box, 0, ["ACE_MainActions"], base_action_deploy_medical]
			call fnc_addAceActionToObjectGlobal;
};

base_action_deploy_medical = ["action_deploy_tent", "Deploy Medical Tent", "", {
		[_target, _player] remoteExec ["fnc_base_deploymedical", 2];
	},
	{ ((({ side _x != civilian } count (crew _target)) == 0) && !isNull ([getPos _target] call fnc_inZone) && isNull (([getPos _target] call fnc_inZone) getVariable ['medical', objNull])) },
	{}, [], {[0, 0, 0]}, 2, [false, false, false, false, false], {}];

fnc_spawn_ifvturret = {
	if (!isServer) exitWith {};
	params["_target", "_player"];
	private _turretpos = "Land_TentLamp_01_suspended_red_F" createVehicle [-5000, -5000, 10000];
	private _turret = "B_APC_Wheeled_01_cannon_F" createVehicle [-5010, -5000, 10000];
	private _turretblock = "BlockConcrete_F" createVehicle [-5000, -5010, 10000];
	_turret attachTo [_turretpos, [0,1,1]];
	_turretblock attachTo [_turret, [0,-1,-1.4]];
	_turretblock setdir 270;
	[_turretpos, false] call BIS_fnc_switchLamp;
	_turretpos setVariable ["boxname", "Static 20mm Cannon", true];
	private _box = [_turret, getPos _player] call fnc_base_containerize;
	clearItemCargoGlobal _turret;
	clearMagazineCargoGlobal _turret;
	clearWeaponCargoGlobal _turret;
	clearBackpackCargoGlobal _turret;
	_turret setObjectTextureGlobal [0,""];
	_turret setObjectTextureGlobal [1,""];
	_turret setObjectTextureGlobal [2,"\dc\bstf\data\dc_apc_wheeled_01_tows_bstf_co.paa"];
	_turret lockDriver true;
	_turret lockCargo true;
	_turret lockInventory true;
	_turret lockTurret [[0,0], true];
	[_turret, 0, ["ACE_MainActions"], base_action_rebox]
		call fnc_addAceActionToObjectGlobal;
	private _turretai = (createGroup [side _player, true]) createUnit ["B_Soldier_F", [0,0,0], [], 0, "NONE"];
	_turretai moveInTurret [_turret, [0]];
	_turretai setBehaviour "COMBAT";
	_turretai allowDamage false;
	_turret setVariable ["parts", [_turretpos, _turretblock, _turretai]];
	_turret addEventHandler ["Deleted", {
		if (isServer) then {
			params ["_turret"];
			private _turretparts = _turret getVariable "parts";
			{
				deleteVehicle _x;
			} foreach _turretparts;
		};
	}];
	for "_i" from 0 to count allCurators -1 do {
		(allCurators select _i) addCuratorEditableObjects [[_turret],true];
	};
};

fnc_spawn_mrkturret = {
	if (!isServer) exitWith {};
	params["_target", "_player"];
	private _turretpos = "Land_TentLamp_01_suspended_red_F" createVehicle [-5000, -5000, 10000];
	private _turret = "B_MBT_01_cannon_F" createVehicle [-5010, -5000, 10000];
	private _turretblock = "BlockConcrete_F" createVehicle [-5000, -5010, 10000];
	_turret attachTo [_turretpos, [0,1,1.5]];
	_turretblock attachTo [_turret, [0,-1,-1.95]];
	_turretblock setdir 270;
	[_turretpos, false] call BIS_fnc_switchLamp;
	_turretpos setVariable ["boxname", "Static 120mm Cannon", true];
	private _box = [_turret, getPos _player] call fnc_base_containerize;
	clearItemCargoGlobal _turret;
	clearMagazineCargoGlobal _turret;
	clearWeaponCargoGlobal _turret;
	clearBackpackCargoGlobal _turret;
	_turret setObjectTextureGlobal [0,""];
	_turret setObjectTextureGlobal [1,"\dc\bstf\data\dc_mbt_01_tow_bstf_co.paa"];
	_turret setObjectTextureGlobal [2,"\dc\bstf\data\dc_mbt_addons_bstf_co.paa"];
	_turret lockDriver true;
	_turret lockCargo true;
	_turret lockInventory true;
	_turret lockTurret [[0,0], true];
	[_turret, 0, ["ACE_MainActions"], base_action_rebox]
		call fnc_addAceActionToObjectGlobal;
	private _turretai = (createGroup [side _player, true]) createUnit ["B_Soldier_F", [0,0,0], [], 0, "NONE"];
	_turretai moveInTurret [_turret, [0]];
	_turretai setBehaviour "COMBAT";
	_turretai allowDamage false;
	_turret setVariable ["parts", [_turretpos, _turretblock, _turretai]];
	_turret addEventHandler ["Deleted", {
		if (isServer) then {
			params ["_turret"];
			private _turretparts = _turret getVariable "parts";
			{
				deleteVehicle _x;
			} foreach _turretparts;
		};
	}];
	for "_i" from 0 to count allCurators -1 do {
		(allCurators select _i) addCuratorEditableObjects [[_turret],true];
	};
};

fnc_base_spendcash = {
	params ["_amount", ["_player", objNull], ["_purchase", ""]];
	private _cash = (missionNamespace getVariable ["base_cash", 0]);
	if (_cash >= _amount) then {
		missionNamespace setVariable ["base_cash", round (_cash - _amount), true];
		if (!isNull _player) then { [_player, format["%1 purchased for $%2.", _purchase, _amount]] remoteExec ["sideChat", 0]; };
		true
	} else {
		false
	};
};

fnc_base_earncash = {
	params ["_amount"];
	missionNamespace setVariable ["base_cash", round ((missionNamespace getVariable ["base_cash", 0]) + _amount), true];
};

fnc_base_infectedkill = {
	missionNamespace setVariable ["base_infectedkilled", (missionNamespace getVariable ["base_infectedkilled", 0]) + 1];
};

fnc_base_buyrespawns = {
	params ["_side", "_respawns", "_player"];
	if ([_respawns * 100] call fnc_base_spendcash) then {
		[Announcer, (format["%1 purchased %2 respawns for %3!", name _player, _respawns, _side])] remoteExec ["globalChat", 0];
		[_side, _respawns] remoteExec ["BIS_fnc_respawnTickets", 2];
	};
};

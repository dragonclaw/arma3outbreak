params ["_newUnit",	"_oldUnit",	"_respawn",	"_respawnDelay"];

private _class = _oldunit getVariable ["class", "_"];

if (count PlayerLoadout > 0) then {
	_newUnit setUnitLoadout PlayerLoadout;
}
else {
	if (!isNull _oldUnit) then {
		_newUnit setUnitLoadout getUnitLoadout [_oldUnit, false];
	};
};
if (!isNull _oldUnit) then {
	if (_oldUnit getVariable ["ACE_hasEarPlugsin", false]) then {
		_newUnit setVariable ["ACE_hasEarPlugsin", true, true];
	};
	_oldUnit setUnitLoadout EmptyPlayerLoadout;
	deleteVehicle _oldUnit;
};
_newUnit enableStamina false;
[_newUnit, _class, false] call fnc_base_becomeClass;
for "_i" from 0 to count allCurators -1 do { 
	[(allCurators select _i), [[player],true]] remoteExec ["addCuratorEditableObjects", 0];
};
if (!isServer) exitWith {};
// Usage: ["Name", Position, Area, Population] call fnc_zone_spawn;
// Returns: Zone Logic or nil
fnc_zone_spawn = {
	params ["_name", "_pos", ["_areasize", 100], ["_population", 100]];
	if (_pos select 0 != 0) then {
		private _zone = zone_group createUnit ["Logic", _pos, [], 0, "NONE"];
		_zone setVariable ["name", _name, true];
		// Technically can get position from the logic object but that can be moved by zeus etc. We'll save it to be safe.
		_zone setVariable ["position", _pos, true];
		_zone setVariable ["areasize", _areasize, true];
		_zone setVariable ["population", _population, true];
		_zone setVariable ["living", _population, true];
		_zone setVariable ["infected", 0, true];
		if (random 1 <= 0.5) then {
			_zone setVariable ["order", (random 0.25) + 0.5, false];
		} else {
			_zone setVariable ["order", 0.8, false];
		};
		_zone setVariable ["group_limit", (ceil (_population / 25)) min zone_group_limit, false];
		_zone setVariable ["events", [], false];

		private _trigger = createTrigger ["EmptyDetector", _pos];
		_trigger setVariable ["zone", _zone, false];
		_trigger setTriggerArea [_areasize + zone_buffer, _areasize + zone_buffer, 0, false];
		_trigger setTriggerActivation ["ANY", "PRESENT", true];
		_trigger setTriggerStatements [
			'isServer && { isPlayer _x } count thisList > 0',
			'[(thisTrigger getVariable "zone")] call fnc_zone_activate',
			''];
		_trigger setTriggerTimeout [0, 0, 0, false];
		_zone setVariable ["trigger", _trigger, false];

		private _untrigger = createTrigger ["EmptyDetector", _pos];
		_untrigger setVariable ["zone", _zone, false];
		_untrigger setTriggerArea [_areasize + (zone_buffer * 2), _areasize + (zone_buffer * 2), 0, false];
		_untrigger setTriggerActivation ["ANY", "PRESENT", true];
		_untrigger setTriggerStatements [
			'isServer && { isPlayer _x } count thisList < 1',
			'[(thisTrigger getVariable "zone")] call fnc_zone_deactivate',
			''];
		_untrigger setTriggerTimeout [30, 30, 30, true];
		_zone setVariable ["untrigger", _untrigger, false];

		private _nameMarker = createMarker [format["base%3%1%2name", floor (_pos select 0), floor (_pos select 1), floor (_pos select 2)],_pos];
		_nameMarker setMarkerShapeLocal "ICON";
		_nameMarker setMarkerTypeLocal "mil_dot";
		_nameMarker setMarkerTextLocal _name;
		_zone setVariable ["nameMarker", _nameMarker, true];

		private _ringMarker = createMarker [format["base%3%1%2ring", floor (_pos select 0), floor (_pos select 1), floor (_pos select 2)],_pos];
		_ringMarker setMarkerShapeLocal "ELLIPSE";
		_ringMarker setMarkerBrushLocal "Border";
		_ringMarker setMarkerSizeLocal [_areasize, _areasize];
		_zone setVariable ["ringMarker", _ringMarker, true];
		[_zone, "ColorBlack"] call fnc_zone_setColor;

		_zone addEventHandler ["Deleted", {
			if (isServer) then {
				params ["_zone"];
				zone_dead_population = zone_dead_population + (_zone getVariable ["population", 0]);
				if (_zone in zone_all) then {
					zone_all deleteAt (zone_all find _zone);
				};
				deleteVehicle (_zone getVariable "trigger");
				deleteVehicle (_zone getVariable "untrigger");
				[_zone] call fnc_zone_deactivate;
				[_zone, "ColorUNKNOWN"] call fnc_zone_setColor;
				deleteMarker (_zone getVariable "ringMarker");
				if (!isNull (_zone getVariable ["medical", objNull])) then {
					deleteVehicle (_zone getVariable "medical");
				};
			};
		}];

		zone_total_population = zone_total_population + _population;
		zone_all pushBack _zone;
		_zone
	};
};

// Usage: [_zone] call fnc_zone_tick;
// Returns: Nothing.
fnc_zone_tick = {
	params ["_zone"];
	private _infected = _zone getVariable ["infected", 0];
	if (_infected >= 1) then {
		private _active = _zone getVariable ["active", false];
		private _living = _zone getVariable ["living", 0];
		private _population = _zone getVariable ["population", 1];
		private _livepop = _infected + _living;
		private _activeunits = ([_zone] call fnc_zone_activetotal);
		private _activeinfected = ([_zone] call fnc_zone_activeinfected);
		private _order = (_zone getVariable "order");
		if (!_active && _living > 0
			&& (random 1) <= ((1 - _order) * 0.1)
			&& ({ alive _x } count zone_spreaders) < zone_spreader_cap
			&& zone_spreader_ready) then {
				zone_spreader_ready = false;
				[] spawn {
					sleep 180;
					zone_spreader_ready = true;
				};
				private _zonecount = count zone_all;
				if (_zonecount > 1) then {
					[_zone, ((zone_all - [_zone]) call fnc_getRandom)] call fnc_zone_spreader;
				};
		} else {
			// Original infection growth logic: ((Infections * 2) * Uninfected Percentage)
			if (isNull (_zone getVariable ["medical", objNull])) then {
				private _infectedrise = _zone getVariable ["infectedrise", 0];
				if (_infectedrise > 0 && _infectedrise <= (_living - ([_zone] call fnc_zone_activenoninfected))) then {
					[_zone, _infectedrise] call fnc_zone_infectliving;
				};
				_zone setVariable ["infectedrise", ((((_infected - _activeinfected) * (_living / _population))
					* (1 - (_activeunits / _livepop))) * zone_infection_coef) max 0];
			} else {
				if (!_active && (_order < zone_chaos_limit) && (random 1 <= ((1 - _order) * 0.02))) then {
					deleteVehicle (_zone getVariable "medical");
					[Announcer, (format["Medical tent in %1 was destroyed.", _zone getVariable ["name", "Somewhere"]])] remoteExec ["globalChat", 0];
				} else {
					[_zone, true] call fnc_zone_killpop;
				};
			};
		};
		if (_living > 0) then {
			if (_active) then {
				_zone setVariable ["order", (_order + 0.05) min 1, false];
			} else {
				_zone setVariable ["order", (_order - ((_infected / _living) / 10)) max 0, false];
			};
		};
		if (_active && _activeunits < ((_zone getVariable ["group_limit", zone_group_limit]) * zone_group_size_limit)) then {
			[_zone] spawn fnc_zone_populate;
		};
		[_zone] call fnc_zone_checkinfection;
		// Debug: Shout the town variables out loud.
		//[Announcer, (format["%1, Pop: %2, Surviving: %3, Infected: %4,  Order: %5 Percent", _zone getVariable ["name", "Somewhere"], _zone getVariable "population", _zone getVariable "living", _zone getVariable "infected", round ((_zone getVariable "order") * 100)])] remoteExec ["globalChat", 0];
	} else {
		[_zone] call fnc_zone_checkdeadpop;
	};
};

// Usage: [_zone] call fnc_zone_activate;
// Returns: Nothing.
fnc_zone_activate = {
	params ["_zone"];
	if (!(_zone getVariable ["active", false])) then {
		_zone setVariable ["active", true, false];
		[_zone] spawn fnc_zone_populate;
		private _zone_events = (_zone getVariable "events");
		for "_i" from 0 to (count _zone_events) -1 do {
			private _task = (_zone_events select _i);
			[_task] call (_task getVariable "activate");
		};
		[_zone] call fnc_zone_checkinfection;
	};
};

// Usage: [_zone] call fnc_zone_deactivate;
// Returns: Nothing.
fnc_zone_deactivate = {
	params ["_zone"];
	if (_zone getVariable ["active", false]) then {
		_zone setVariable ["active", false, false];
		private _activegroups = _zone getVariable["activegroups", []];
		for "_i" from count _activegroups -1 to 0 step -1 do {
			private _group = _activegroups select _i;
			private _units = (units _group);
			for "_u" from count _units -1 to 0 step -1 do {
				(_units select _u) spawn { deleteVehicle _this; };
			};
			[_zone, _group] spawn {
				params["_zone", "_group"];
				private _activegroups = _zone getVariable["activegroups", []];
				uisleep 10;
				_activegroups deleteAt (_activegroups find _group);
				deleteGroup _group;
			};
		};
		[_zone, "ColorBlack"] call fnc_zone_setColor;
		private _zone_events = (_zone getVariable "events");
		for "_i" from 0 to (count _zone_events) -1 do {
			private _task = (_zone_events select _i);
			[_task] call (_task getVariable "deactivate");
		};
	};
};

// Usage: [_zone, _amount] call fnc_zone_infectliving;
// Returns: Nothing.
fnc_zone_infectliving = {
	params ["_zone", "_amount"];
	private _remainder = _zone getVariable ["remainder", 0];
	_zone setVariable ["remainder", ((_remainder - (floor _remainder)) + (_amount - (floor _amount))), false];
	_amount = (floor _amount) + (floor _remainder); // Sanity check because lmao @ 0.6 infected.
	private _newliving = ((_zone getVariable ["living", 0]) - _amount) max 0;
	private _newinfected = ((_zone getVariable ["infected", 0]) + _amount) max 0;
	_zone setVariable ["infected", _newinfected, true];
	_zone setVariable ["living", _newliving, true];
	if (_newliving < 1) then {
		_zone setVariable ["order", 0, false];
	};
	//[Announcer, (format["%1, Leftover: %2", _zone getVariable ["name", "Somewhere"], _zone getVariable ["remainder", 0]])] remoteExec ["globalChat", 0];
};

// Usage: [_zone, _isInfected (optional), _amount (optional)] call fnc_zone_killpop;
// Returns: Nothing.
fnc_zone_killpop = {
	params ["_zone", ["_isInfected", false], ["_amount", 1]];
	if (_isInfected) then {
		private _newinfected = ((_zone getVariable ["infected", 0])
			- _amount) max 0;
		_zone setVariable ["infected", _newinfected, true];
		if (!([_zone] call fnc_zone_checkdeadpop)) then {
			[_zone] call fnc_zone_checkinfection;
		};
	} else {
		private _newliving = ((_zone getVariable ["living", 0])
			- _amount) max 0;
		_zone setVariable ["living", _newliving, true];
		[_zone] call fnc_zone_checkdeadpop;
		if (_newliving < 1) then {
			_zone setVariable ["order", 0, false];
		};
	};
};

// Usage: [_zone] call fnc_zone_checkdeadpop;
// Returns: Boolean
fnc_zone_checkdeadpop = {
	private _isDead = false;
	private _alive = (_zone getVariable ["living", 1]) + (_zone getVariable ["infected", 1]);
	if (_alive < 1) then {
		_isDead = true;
		[Announcer, (format["%1 has been wiped out.", _zone getVariable ["name", "Somewhere"]])] remoteExec ["globalChat", 0];
		deleteVehicle _zone;
	};
	_isDead
};

// Usage: [_zone] call fnc_zone_checkdeadpop;
// Returns: Nothing.
fnc_zone_checkinfection = {
	if (_zone getVariable ["active", false]) then {
		private _color = markerColor (_zone getVariable "nameMarker");
		private _newcolor = "ColorCivilian";
		switch (true) do {
			case (!(isNull (_zone getVariable ["medical", objNull]))): {
				_newcolor = "ColorBLUFOR";
			};
			case ((_zone getVariable ["infected", 0]) > 0): {
				_newcolor = "ColorOPFOR";
			};
			case ((_zone getVariable ["order", 1]) < zone_chaos_limit): {
				_newcolor = "ColorIndependent";
			};
		};
		if (_color != _newcolor) then {
			[_zone, _newcolor] call fnc_zone_setColor;
		};
	};
};

// Usage: [_zone] call fnc_zone_activetotal;
// Returns: Number of units currently in zone.
fnc_zone_activetotal = {
	params ["_zone"];
	private _activecount = 0;
	if (_zone getVariable ["active", false]) then {
		private _activegroups = _zone getVariable["activegroups", []];
		for "_i" from count _activegroups -1 to 0 step -1 do {
			_activecount = _activecount + ({ alive _x } count (units (_activegroups select _i)));
		};
	};
	_activecount
};

// Usage: [_zone] call fnc_zone_activeinfected;
// Returns: Number of infected units currently in zone.
fnc_zone_activeinfected = {
	params ["_zone"];
	private _activecount = 0;
	if (_zone getVariable ["active", false]) then {
		private _activegroups = _zone getVariable["activegroups", []];
		for "_i" from count _activegroups -1 to 0 step -1 do {
			private _group = (_activegroups select _i);
			if (side _group == side_infected) then {
				_activecount = _activecount + ({ alive _x } count (units _group));
			};
		};
	};
	_activecount
};

// Usage: [_zone] call fnc_zone_activenoninfected;
// Returns: Number of non-infected units currently in zone.
fnc_zone_activenoninfected = {
	params ["_zone"];
	private _activecount = 0;
	if (_zone getVariable ["active", false]) then {
		private _activegroups = _zone getVariable["activegroups", []];
		for "_i" from count _activegroups -1 to 0 step -1 do {
			private _group = (_activegroups select _i);
			if (side _group != side_infected) then {
				_activecount = _activecount + ({ alive _x } count (units _group));
			};
		};
	};
	_activecount
};

// Usage: ["_zoneName"] call fnc_zone_byName;
// Returns: Zone
fnc_zone_byName = {
	params ["_zoneName"];
	private _zone = objNull;
	for "_i" from count zone_all -1 to 0 step -1 do {
		private _z = (zone_all select _i);
		if (_z getVariable ["Name", "_"] == _zoneName) then {
			_zone = _z;
		};
	};
	_zone
};

// Usage: [_zone, _position] call fnc_zone_move;
// Returns: Nothing.
fnc_zone_move = {
	params ["_zone", "_pos"];
	_zone setPos _pos;
	_zone setVariable ["position", _pos, false];
	(_zone getVariable "trigger") setPos _pos;
	(_zone getVariable "untrigger") setPos _pos;
	(_zone getVariable "nameMarker") setMarkerPos _pos;
	(_zone getVariable "ringMarker") setMarkerPos _pos;
};

// Usage: [_zone, _areasize] call fnc_zone_resize;
// Returns: Nothing.
fnc_zone_resize = {
	params ["_zone", "_areasize"];
	_zone setVariable ["areasize", _areasize, false];
	(_zone getVariable "trigger") setTriggerArea [_areasize + zone_buffer, _areasize + zone_buffer, 0, false];
	(_zone getVariable "untrigger") setTriggerArea [_areasize + (zone_buffer * 2), _areasize + (zone_buffer * 2), 0, false];
	(_zone getVariable "ringMarker") setMarkerSize [_areasize, _areasize];
};

// Usage: [_zone, "_color"] call fnc_zone_setColor;
// Returns: Nothing.
fnc_zone_setColor = {
	params ["_zone", "_color"];
	(_zone getVariable "nameMarker") setMarkerColor _color;
	(_zone getVariable "ringMarker") setMarkerColor _color;
};
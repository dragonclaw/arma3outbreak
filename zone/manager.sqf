if (!isServer) exitWith {};

[] spawn {
	sleep 530;
	sleep random 1200;
	zone_group setVariable ["eventTick", true, false];
};

while { true } do {
	sleep 60;
	private _global_infected = 0;
	private _global_surviving = 0;
	private _tick_earnings = (missionNamespace getVariable ["base_infectedkilled", 0]) / 5;
	for "_i" from 0 to count zone_all -1 do {
		private _zone = zone_all select _i;
		[_zone] call fnc_zone_tick;
		_global_infected = _global_infected + (_zone getVariable ["infected", 0]);
		private _zone_surviving = _zone getVariable ["living", 0];
		_global_surviving = _global_surviving + _zone_surviving;
		if (!(isNull (_zone getVariable ["medical", objNull]))) then {
			_tick_earnings = _tick_earnings + (_zone_surviving / 4);
		} else {
			_tick_earnings = _tick_earnings + (_zone_surviving / 10);
		};
	};
	switch (true) do {
		case (_global_surviving < 1 && _global_infected < 1): {
			["mainObjective","CANCELED"] call BIS_fnc_taskSetState;
			[] spawn {
				uisleep 3;
				["end3", true, true, true, true] remoteExec ["BIS_fnc_endMission"];
			};
		};
		case (_global_infected < 1): {
			["mainObjective","SUCCEEDED"] call BIS_fnc_taskSetState;
			[] spawn {
				uisleep 3;
				["end1", true, true, true, true] remoteExec ["BIS_fnc_endMission"];
			};
		};
		default {
			[floor (_tick_earnings / base_tick_divisor)] call fnc_base_earncash;
		};
	};
	if (zone_group getVariable ["eventTick", false]) then {
		// One in a hundred chance for two consecutive events!
		if ((random 1) >= 0.01) then {
			zone_group setVariable ["eventTick", false, false];
			[] spawn {
				sleep 600;
				sleep random 1200;
				zone_group setVariable ["eventTick", true, false];
			};
		};
		private _zonePicks = [];
		for "_i" from 0 to count zone_all -1 do {
			private _zone = zone_all select _i;
			if (_zone getVariable "infected" > 0) then {
				_zonePicks pushBackUnique _zone;
			};
		};
		if (count _zonePicks > 0) then {
			switch (["hvt", "rescue", "supply"] call fnc_getRandom) do {
				case "hvt": {
					[_zonePicks call fnc_getRandom] spawn fnc_zone_event_hvt;
				};
				case "horde": {
					[_zonePicks call fnc_getRandom, zone_all call fnc_getRandom] spawn fnc_zone_event_horde;
				};
				case "rescue": {
					[_zonePicks call fnc_getRandom] spawn fnc_zone_event_rescue;
				};
				case "supply": {
					[_zonePicks call fnc_getRandom] spawn fnc_zone_event_supply;
				};
			};
		};
	};
};
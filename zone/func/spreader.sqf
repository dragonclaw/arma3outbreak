// Spreads infection from zone to zone.
// Usage [_from, _target] spawn fnc_zone_spreader;
// Returns: Nada.
_this spawn {
	params ["_from", "_target"];
	sleep (random 120);

	private _fromPos = [random (_from getVariable "areasize"), random 360, 90, (_from getVariable "position")] call fnc_polToCart;
	private _newloc = [];
	private _pos = _fromPos;
	private _j = 0;
	while { count _newloc < 1 && _j < 99 } do {
		_newloc = [_pos, 0, 200, 5, 0, 0, 0] call BIS_fnc_findSafePos;
		_j = _j + 1;
	};
	if (count _newloc > 1) then { _pos = _newloc; };
	private _vehicle = (["C_Tractor_01_F", "C_Hatchback_01_F", "C_Hatchback_01_sport_F", "C_Offroad_02_unarmed_F", "C_Truck_02_transport_F", "C_Offroad_01_repair_F", "C_SUV_01_F", "C_Quadbike_01_F", "C_Van_01_box_F", "C_Van_02_vehicle_F"] call fnc_getRandom) createVehicle _pos;
	private _group = createGroup side_looters;
	private _unit = _group createUnit [(zone_faction_units select zone_faction_looter) call fnc_getRandom, _fromPos, [], 0, "NONE"];
	[_unit, zone_faction_looter] spawn fnc_zone_gearUnit;
	_unit setSkill random 1;
	_unit moveInDriver _vehicle;
	_unit addEventHandler ["Killed", {
		params ["_unit", "_killer", "_instigator", "_useEffects"];
		if (local _unit) then {
			if (isPlayer _instigator) then {
				[10] remoteExec ["fnc_base_earncash", 2];
			};
			private _task = (_unit getVariable "task");
			[(_task getVariable "name"),"SUCCEEDED"] call BIS_fnc_taskSetState;
			[(_task getVariable "name"),objNull] call BIS_fnc_taskSetDestination;
			if (_task in zone_events) then {
				zone_events deleteAt (zone_events find _task);
			};
			[(_task getVariable "name")] spawn {
				uisleep 30;
				[(_this select 0), side_players, true] call BIS_fnc_deleteTask;
			};
			deleteVehicle _task;
			(_unit getVariable ["vehicle", objNull]) spawn {
				sleep (random 900);
				_this setDamage 0.5;
				sleep 60;
				_this setDamage 0.66;
				sleep 60;
				_this setDamage 0.75;
				sleep 60;
				_this setDamage 1;
			};
			[_unit] spawn {
				params ["_unit"];
				if (!isNil "_unit") then {
					if (_unit in zone_spreaders) then {
						zone_spreaders deleteAt (zone_spreaders find _unit);
					};
				} else {
					for "_i" from (count zone_spreaders) -1 to 0 do {
						private _spreader = (zone_spreaders select _i);
						if (isNull _spreader) then {
							zone_spreaders deleteAt _i;
						};
					};
				};
				sleep 120;
				deleteVehicle _unit;
			};
		};
	}];
	_unit setVariable ["target", _target, false];
	_unit setVariable ["vehicle", _vehicle, false];
	_vehicle addEventHandler ["Killed", {
		params ["_unit", "_killer", "_instigator", "_useEffects"];
		if (local _unit) then {
			_unit spawn {
				sleep 120;
				deleteVehicle _this;
			};
		};
	}];
	zone_spreaders pushBack _unit;
	_group deleteGroupWhenEmpty true;
	private _targPos = (_target getVariable "position");
	private _waypoint = _group addWaypoint [_targPos, (_target getVariable "areasize")];
	_waypoint setWaypointCompletionRadius 10;
	_waypoint setWaypointStatements ["true", "if (isServer) then {
		[(this getVariable 'target'), 1] spawn fnc_zone_infectliving;
		this spawn {
			private _task = (_this getVariable 'task');
			[(_task getVariable 'name'),'FAILED'] call BIS_fnc_taskSetState;
			[(_task getVariable 'name'),objNull] call BIS_fnc_taskSetDestination;
			if (_task in zone_events) then { zone_events deleteAt (zone_events find _task); };
			[(_task getVariable 'name')] spawn {
				uisleep 30;
				[(_this select 0), side_players, true] call BIS_fnc_deleteTask;
			};
			deleteVehicle _task;
			sleep ((random 10) + 5);
			deleteVehicle (_this getVariable 'vehicle');
			deleteVehicle _this;
		} };"];
	private _task = zone_group createUnit ["Logic", [0,0,0], [], 0, "NONE"];
	_unit setVariable ["task", _task, false];
	private _taskName = format["spreader_%1", (ceil (random 999999))];
	_task setVariable ["name", _taskName, false];
	zone_events pushBackUnique _task;
	[side_players, _taskName, ["Stop the car from reaching it's destination.", "Stop The Car", "Spreader"], [_unit, true], "AUTOASSIGNED", 2, true, "car", false] call BIS_fnc_taskCreate;
	for "_i" from 0 to count allCurators -1 do {
		(allCurators select _i) addCuratorEditableObjects [[_vehicle],TRUE];
	};
};
// Usage: [_group, _position, _skill] spawn fnc_zone_spawnUnit;
// Returns: Unit.
params ["_group", "_position", "_skill"];
if (isNull _group) exitWith {}; // We're "async", _group might have gone away.

private _faction = (_group getVariable ["faction", 0]);
private _unitType = (zone_faction_units select _faction) call fnc_getRandom;
private _unit = _group createUnit [_unitType, _position, [], (random 8) + 2, "NONE"];
[_unit, getPos _unit] call KK_fnc_setPosAGLS;
[_unit, _faction] spawn fnc_zone_gearUnit;
_unit setSkill (random _skill);
if ((side _group) == side_infected) then {
_unit addEventHandler ["Killed", {
	params ["_unit", "_killer", "_instigator", "_useEffects"];
	if (isServer) then {
		private _group = group _unit;
		private _zone = _group getVariable "zone";
		[_zone, true] call fnc_zone_killpop;
		if (({ alive _x } count units _group) < 2) then {
			private _newunits = ((_zone getVariable ["infected", 0]) - ([_zone] call fnc_zone_activeinfected)) min (ceil (random 8));
			if (_newunits > 0) then {
				private _position = [random (_zone getVariable ["areasize", 100]), random 360, 90, _zone getVariable "position"] call fnc_polToCart;
				for "_i" from 0 to _newunits -1 do {
					[_group, _position, 1] spawn fnc_zone_spawnUnit;
				};
			};
		};
		[] call fnc_base_infectedkill;
		[50] call fnc_base_earncash;
		_unit spawn { sleep 120; deleteVehicle _this; };
	};
}];
} else {
	if (side _unit == side_looters) then {
		_unit addEventHandler ["Hit", {
			params ["_unit", "_source", "_damage", "_instigator"];
			if (alive _unit && !(_unit getVariable ["ACE_isSurrendering", false]) && !(_unit getVariable ["ACE_isHandcuffed", false])) then {
				if (side _instigator == side_players) then {
					if (random 1 <= damage _unit) then {
						[_unit, true] call ACE_captives_fnc_setSurrendered;
					};
				};
			};
		}];
	};
	_unit addEventHandler ["Killed", {
		params ["_unit", "_killer", "_instigator", "_useEffects"];
		if (isServer) then {
			private _zone = (group _unit) getVariable "zone";
			[_zone] call fnc_zone_killpop;
			_unit spawn { sleep 120; deleteVehicle _this; };
			if ((side _unit) == side_looters) then {
				_zone setVariable ["order", ((_zone getVariable "order") + 0.04) min 1];
				if (isPlayer _instigator) then {
					[10] call fnc_base_earncash;
				};
			} else {
				_zone setVariable ["order", ((_zone getVariable "order") - 0.02) max 0];
			};
		};
	}];
	_unit addEventHandler ["Deleted", {
		params ["_unit"];
		if (alive _unit) then {
			switch (true) do {
				case ((_unit getVariable ["ryanzombiesinfected", 0]) > 0): {
					private _zone = (group _unit) getVariable "zone";
					[_zone, 1] call fnc_zone_infectliving;
				};
				case ((side _unit == side_looters) && (_unit getVariable ["ACE_isHandcuffed", false])): {
					[20] call fnc_base_earncash;
					private _zone = (group _unit) getVariable "zone";
					_zone setVariable ["order", ((_zone getVariable "order") + 0.1) min 1];
				};
			};
		};
	}];
};
for "_i" from 0 to count allCurators -1 do {
	(allCurators select _i) addCuratorEditableObjects [[_unit],true];
};

_unit
// Usage: [_zone] spawn fnc_zone_populate;
// Returns: Nothing.
params ["_zone"];

private _infected = (_zone getVariable ["infected", 0]);
private _living = (_zone getVariable ["living", 0]);
if (([_zone] call fnc_zone_activetotal) < (_living + _infected)) then {
	private _activegroups = _zone getVariable ["activegroups", []];
	private _activegroupcount = count _activegroups;
	private _infected_remaining = (_infected - ([_zone] call fnc_zone_activeinfected));
	private _noninfected_remaining = (_living - ([_zone] call fnc_zone_activenoninfected));
	if (_activegroupcount > 0) then {
		for "_i" from _activegroupcount -1 to 0 step -1 do {
			private _group = (_activegroups select _i);
			private _side = side _group;
			if ((_side == side_infected && _infected_remaining > 0) || _noninfected_remaining > 0) then {
				private _units = units _group;
				if (isNull _group || { alive _x } count _units < 1) then {
					_activegroups deleteAt _i;
					_group spawn { uisleep 10; deleteGroup _this; };
				} else {
					private _maxnewunits = (zone_group_size_limit - count _units);
					if (_side == side_infected) then {
						_maxnewunits = _maxnewunits min _infected_remaining;
					} else {
						_maxnewunits = _maxnewunits min _noninfected_remaining;
					};
					if (_maxnewunits > 0) then {
						private _newunits = ((ceil random 4) min _maxnewunits);
						private _position = [random (_zone getVariable ["areasize", 100]), random 360, 90, _zone getVariable "position"] call fnc_polToCart;
						for "_j" from 0 to _newunits -1 do {
							[_group, _position, 1] spawn fnc_zone_spawnUnit;
						};
						if (_side == side_infected) then {
							_infected_remaining = _infected_remaining - _newunits;
						} else {
							_noninfected_remaining = _noninfected_remaining - _newunits;
						};
					};
				};
			};
		};
	};
	private _group_limit = (_zone getVariable ["group_limit", zone_group_limit]);
	if (_activegroupcount < _group_limit) then {
		private _infectedcap = ceil (_group_limit * (_infected / (_living + _infected)));
		private _noninfectedcap = (_group_limit - _infectedcap);
		private _infectedgroupcount = ({ side _x == side_infected } count _activegroups);
		private _noninfectedgroupcount = ({ side _x != side_infected } count _activegroups);
		if (_infectedgroupcount < _infectedcap && _infected_remaining > 0) then {
			private _newgroupcount = (ceil (_infected_remaining / 8))
				min ((ceil (random zone_max_new_groups))
				min (_infectedcap - _infectedgroupcount));
			for "_i" from 0 to _newgroupcount -1 do {
				if (_infected_remaining > 0) then {
					private _unitcount = (ceil random 8) min _infected_remaining;
					[_zone, side_infected, _unitcount, (random 0.5) + 0.5] spawn fnc_zone_spawnGroup;
					_infected_remaining = _infected_remaining - _unitcount;
				};
			};
		};
		if (_noninfectedgroupcount < _noninfectedcap && _noninfected_remaining > 0) then {
			private _newgroupcount = (ceil (_noninfected_remaining / 4))
				min ((ceil (random zone_max_new_groups))
				min (_noninfectedcap - _noninfectedgroupcount));
			for "_i" from 0 to _newgroupcount -1 do {
				if (_noninfected_remaining > 0) then {
					private _side = side_looters;
					if (random 1 <= _zone getVariable ["order", 0]) then { _side = side_players; };
					private _unitcount = (ceil random 4) min _noninfected_remaining;
					[_zone, _side, _unitcount, (random 0.5) + 0.5] spawn fnc_zone_spawnGroup;
					_noninfected_remaining = _noninfected_remaining - _unitcount;
				};
			};
		};
	};
};
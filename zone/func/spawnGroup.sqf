// Usage: [] spawn fnc_zone_spawnGroup;
// Returns: Nothing.
params ["_zone", "_side", "_size", "_skill"];

if (isServer) then {
	private _group = createGroup _side;
	if (isNull _group) exitWith {}; // No group, no spawn.
	_group setVariable ["zone", _zone, false];
	private _weightedFaction = 1;
	if ((random 1) <= 0.1) then { _weightedFaction = 0; };
	switch (_side) do {
		case side_players: {
			_group setVariable ["faction", (zone_side_factions select 1) select _weightedFaction, false];
		};
		case side_looters: {
			_group setVariable ["faction", (zone_side_factions select 2) select _weightedFaction, false];
		};
		default {
			_group setVariable ["faction", (zone_side_factions select 0) select _weightedFaction, false];
		};
	};
	private _position = [random (_zone getVariable ["areasize", 100]), random 360, 90, _zone getVariable "position"] call fnc_polToCart;
	private _allhandles = [];
	for "_s" from 0 to _size -1 do {
		private _handle = [_group, _position, _skill] spawn fnc_zone_spawnUnit;
		_allhandles pushBack _handle;
	};
	[_group, _allhandles] spawn {
		params ["_group", "_allhandles"];
		waitUntil { _allhandles call fnc_allScriptsDone };
		private _rankMulti = 500;
		private _rank = _rankMulti * floor (count units _group / 2);
		private _units = units _group;
		for "_i" from 0 to count _units -1 do {
			(_units select _i) setUnitRank (_rank call fnc_getRank);
			if (_rank > 0) then {
				if ((_rank - _rankMulti) > 0) then { _rank = _rank - _rankMulti; }
				else { _rank = 0; };
			};
		};
	};
	private _groups = _zone getVariable ["activegroups", []];
	_groups pushBack _group;
	_zone setVariable ["activegroups", _groups, false];
};
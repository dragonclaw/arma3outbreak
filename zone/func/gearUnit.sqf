params ["_unit", "_faction"];
private _zone = ([getPos _unit] call fnc_inZone);
private _extraguns = false;
if (!isNull _zone) then {
	_extraguns = _zone getVariable ["survivorguns", false];
};
switch _faction do {
	case 2: {
		private _headgear = "H_MilCap_gen_F";
		private _uniform = "U_B_GEN_Soldier_F";
		private _vest = "V_TacVest_gen_F";
		private _rifle = "arifle_SPAR_01_blk_F";
		private _rlazlite = "acc_flashlight";
		private _roptic = "optic_Holosight_blk_F";
		private _rmag = "30Rnd_556x45_Stanag_Red";
		private _rpod = "_";
		private _medic = false;

		switch (ceil (random 6)) do {
			case 6: {
				_headgear = "H_PASGT_basic_black_F";
				_uniform = "U_B_GEN_Commander_F";
				_vest = "V_PlateCarrier1_blk";
				_rifle = "arifle_SPAR_03_blk_F";
				_rlazlite = "acc_pointer_IR";
				_roptic = "optic_Nightstalker";
				_rmag = "20Rnd_762x51_Mag";
				_rpod = "bipod_01_F_blk";
				_unit linkItem "ACE_NVG_Gen4_Black";
			};
			case 5: {
				_headgear = "H_PASGT_basic_black_F";
				_vest = "V_PlateCarrier1_blk";
				_rlazlite = "acc_pointer_IR";
				_unit linkItem "ACE_NVG_Gen4_Black";
				_medic = true;
			};
			case 4: {
				_headgear = "H_PASGT_basic_black_F";
				_vest = "V_PlateCarrier1_blk";
				_rlazlite = "acc_pointer_IR";
				_unit linkItem "ACE_NVG_Gen4_Black";
			};
			case 3: {
				_headgear = "H_Beret_gen_F";
				_uniform = "U_B_GEN_Commander_F";
			};
			case 2: {
				_medic = true;
			};
			default {};
		};

		removeAllWeapons _unit;
		removeAllItems _unit;
		removeAllAssignedItems _unit;
		removeUniform _unit;
		removeVest _unit;
		removeBackpack _unit;
		removeHeadgear _unit;

		_unit addWeapon _rifle;
		_unit addPrimaryWeaponItem _rlazlite;
		_unit addPrimaryWeaponItem _roptic;
		_unit addPrimaryWeaponItem _rmag;
		if (_rpod != "_") then { _unit addPrimaryWeaponItem _rpod; };
		_unit addWeapon "hgun_P07_blk_F";
		_unit addHandgunItem "16Rnd_9x21_Mag";

		_unit forceAddUniform _uniform;
		_unit addVest _vest;
		_unit addHeadgear _headgear;

		_unit addItemToUniform "FirstAidKit";
		_unit addItemToUniform "ACE_EarPlugs";
		_unit addItemToUniform "ACE_Flashlight_XL50";
		_unit addItemToUniform _rmag;
		_unit addItemToUniform "SmokeShell";
		_unit addItemToUniform "ACE_M84";
		for "_i" from 1 to 2 do {_unit addItemToUniform "16Rnd_9x21_Mag";};
		_unit addItemToVest "SmokeShell";
		_unit addItemToVest "ACE_HandFlare_White";
		for "_i" from 1 to 6 do {_unit addItemToVest _rmag;};
		for "_i" from 1 to 3 do {_unit addItemToVest "16Rnd_9x21_Mag";};
		for "_i" from 1 to 2 do {_unit addItemToVest "ACE_M84";};

		_unit linkItem "ItemMap";
		_unit linkItem "ItemCompass";
		_unit linkItem "ItemWatch";
		_unit linkItem "ItemRadio";

		if (_medic) then {
			_unit addBackpack "B_AssaultPack_blk";
			_unit addItemToBackpack "Medikit";
			for "_i" from 1 to 8 do { _unit addItemToBackpack "FirstAidKit"; };
			_unit setUnitTrait ["medic",true];
		};
	};
	case 3: {
		removeAllWeapons _unit;
		removeAllItems _unit;
		removeAllAssignedItems _unit;
		removeUniform _unit;
		removeVest _unit;
		removeBackpack _unit;
		removeHeadgear _unit;
		removeGoggles _unit;

		private _vest = "_";
		if (_extraguns) then {
			_vest = ["V_Rangemaster_belt", "V_Chestrig_blk", "V_BandollierB_cbr",
				"V_Chestrig_rgr", "V_Chestrig_khk", "V_Chestrig_oli",
				"V_TacChestrig_cbr_F", "V_TacChestrig_grn_F", "V_TacChestrig_oli_F",
				"V_Platecarrier1_blk", "V_Platecarrier1_rgr_noflag_F", "V_Platecarrier2_rgr_noflag_F",
				"V_LegStrapBag_black_F", "V_LegStrapBag_coyote_F", "V_LegStrapBag_olive_F"] call fnc_getRandom;
		} else {
			if (random 1 <= 0.6) then {
				_vest = ["V_Rangemaster_belt", "V_BandollierB_blk", "V_BandollierB_cbr",
					"V_BandollierB_rgr", "V_BandollierB_khk", "V_BandollierB_oli",
					"V_TacChestrig_cbr_F", "V_TacChestrig_grn_F", "V_TacChestrig_oli_F",
					"V_Pocketed_black_F", "V_Pocketed_coyote_F", "V_Pocketed_olive_F",
					"V_LegStrapBag_black_F", "V_LegStrapBag_coyote_F", "V_LegStrapBag_olive_F"] call fnc_getRandom;
			};
		};

		private _hasgun = false;
		private _hgun = "_";
		private _hgunmag = "_";
		if (random 1 <= 0.2) then {
			_hasgun = true;
			_hgun = [["hgun_Pistol_heavy_02_F", "6Rnd_45ACP_Cylinder"],
				["hgun_ACPC2_F", "9Rnd_45ACP_Mag"],
				["hgun_Pistol_heavy_01_F", "11Rnd_45ACP_Mag"],
				["hgun_Pistol_heavy_01_green_F", "11Rnd_45ACP_Mag"],
				["hgun_Pistol_01_F", "10Rnd_9x21_Mag"],
				["hgun_Rook40F", "16Rnd_9x21_Mag"],
				["hgun_P07_F", "16Rnd_9x21_Mag"],
				["hgun_P07_blk_F", "16Rnd_9x21_Mag"],
				["hgun_P07_khk_F", "16Rnd_9x21_Mag"],
				["hgun_Pistol_Signal_F", "6Rnd_GreenSignal_F"]] call fnc_getRandom;
			_hgunmag = (_hgun select 1);
			_hgun = (_hgun select 0);
			_unit addWeapon _hgun;
			_unit addHandgunItem _hgunmag;
		};

		_unit addHeadgear (["H_Bandanna_surfer", "H_Bandanna_surfer_grn",
				"H_Bandanna_surfer_blk", "H_Cap_blk", "H_Cap_blu",
				"H_Cap_blk_CMMG", "H_Cap_grn", "H_Cap_grn_BI",
				"H_Cap_red", "H_Cap_surfer", "H_Cap_white_IDAP_F",
				"H_Construction_basic_yellow_F", "H_Hat_blue", "H_Hat_brown",
				"H_Hat_checker", "H_Hat_grey", "H_Hat_tan", "H_StrawHat_dark",
				"H_Hat_Safari_sand_F", "H_Hat_Tinfoil_F", "H_WirelessEarpiece_F"] call fnc_getRandom);

		_unit forceAddUniform (["U_C_Man_casual_4_F", "U_OrestesBody",
				"U_Marshal", "U_C_Mechanic_01_F", "U_C_Poor_1",
				"U_C_Uniform_Farmer_01_F", "U_C_Poloshirt_redwhite",
				"U_C_Man_casual_2_F", "U_C_IDAP_Man_Jeans_F"] call fnc_getRandom);

		_unit addItemToUniform "ACE_Cellphone";
		_unit addItemToUniform "ACE_WaterBottle";
		_unit addItemToUniform "ACE_HandFlare_White";
		if (_hasgun) then {
			for "_i" from 1 to 2 do {_unit addItemToUniform _hgunmag;};
		};
		for "_i" from 1 to 2 do {_unit addItemToUniform "ACE_Chemlight_HiGreen";};

		if (_vest != "_") then {
			_unit addVest _vest;
			_unit addItemToVest "ACE_Flashlight_KSF1";
			for "_i" from 1 to 2 do {_unit addItemToVest "ACE_Chemlight_HiGreen";};
			if (_hasgun) then {
				for "_i" from 1 to 6 do {_unit addItemToVest _hgunmag;};
			};
		};

		if (random 0 <= 0.4) then {
			_unit addGoggles (["G_Respirator_blue", "G_Respirator_white", "G_Respirator_yellow",
				"G_Aviator", "G_Shades_Black", "G_Shades_Green", "G_Sport_Blackred",
				"G_Squares", "G_Spectacles"] call fnc_getRandom);
		};

		_unit linkItem "ItemMap";
		_unit linkItem "ItemCompass";
		_unit linkItem "ItemWatch";

		private _backpackChoices = ["B_Messenger_Black_F", "B_Messenger_Coyote_F",
			"B_Messenger_Gray_F", "B_Messenger_Olive_F", "B_CivilianBackpack_01_Sport_Blue_F",
			"B_CivilianBackpack_01_Sport_Green_F", "B_CivilianBackpack_01_Sport_Red_F"];
		if (random 1 <= 0.2) then {
			_unit addBackpack (_backpackChoices call fnc_getRandom);
			_unit addItemToBackpack "Medikit";
			for "_i" from 1 to 6 do { _unit addItemToBackpack "FirstAidKit"; };
			_unit setUnitTrait ["medic",true];
			if (_hasgun) then {
				for "_i" from 1 to 2 do {_unit addItemToBackpack _hgunmag;};
			};
		} else {
			if (random 1 <= 0.2) then {
				_unit addBackpack (_backpackChoices call fnc_getRandom);
				for "_i" from 1 to 2 do { _unit addItemToBackpack "FirstAidKit"; };
				if (_hasgun) then {
					for "_i" from 1 to 6 do {_unit addItemToBackpack _hgunmag;};
				};
				if (random 1 <= 0.01) then {
					_unit addItemToBackpack "ACE_Banana";
				};
			}
		};
		if (_extraguns) then {
			private _rifle = ([["arifle_Mk20_F", "30Rnd_556x45_Stanag_green", "optic_mrco", "_"],
				["arifle_Mk20C_F", "30Rnd_556x45_Stanag_green", "optic_aco_grn", "_"],
				["arifle_Mk20_plain_F", "30Rnd_556x45_Stanag_Sand_green", "optic_mrco", "_"],
				["arifle_Mk20C_plain_F", "30Rnd_556x45_Stanag_Sand_green", "optic_aco_grn", "_"],
				["arifle_SPAR_01_khk_F", "30Rnd_556x45_Stanag_green", "optic_Holosight_khk_F", "bipod_01_f_khk"],
				["arifle_SPAR_02_khk_F", "30Rnd_556x45_Stanag_green", "optic_Holosight_khk_F", "bipod_01_f_khk"],
				["arifle_SPAR_03_khk_F", "20Rnd_762x51_Mag", "optic_Nightstalker", "bipod_01_f_khk"],
				["arifle_SPAR_01_snd_F", "30Rnd_556x45_Stanag_Sand_green", "optic_Holosight", "bipod_01_f_snd"],
				["arifle_SPAR_02_snd_F", "30Rnd_556x45_Stanag_Sand_green", "optic_Holosight", "bipod_01_f_snd"],
				["arifle_SPAR_03_snd_F", "20Rnd_762x51_Mag", "optic_nvs", "bipod_01_F_snd"],
				["arifle_TRG20_F", "30Rnd_556x45_Stanag_green", "optic_aco", "_"],
				["arifle_TRG21_F", "30Rnd_556x45_Stanag_green", "optic_hamr", "_"],
				["arifle_AK12_lush_F", "30rnd_762x39_AK12_Lush_Mag_F", "optic_arco_lush_F", "bipod_02_f_lush"],
				["arifle_AK12U_lush_F", "30rnd_762x39_AK12_Lush_Mag_F", "optic_arco_lush_F", "bipod_02_f_lush"],
				["arifle_AK12_arid_F", "30rnd_762x39_AK12_Arid_Mag_F", "optic_arco_arid_F", "bipod_02_f_arid"],
				["arifle_AK12U_arid_F", "30rnd_762x39_AK12_Arid_Mag_F", "optic_arco_arid_F", "bipod_02_f_arid"]] call fnc_getRandom);
			private _rpod = _rifle select 3;
			private _roptic = _rifle select 2;
			private _rmag = _rifle select 1;
			_rifle = _rifle select 0;
			_unit addItemToUniform "MiniGrenade";
			_unit addItemToVest "HandGrenade";
			for "_i" from 1 to 6 do {_unit addItemToVest _rmag;};
			_unit addItemToVest "SmokeShell";
			_unit addItemToVest "SmokeShellGreen";
			_unit addWeapon _rifle;
			_unit addPrimaryWeaponItem "acc_flashlight";
			_unit addPrimaryWeaponItem _roptic;
			_unit addPrimaryWeaponItem _rmag;
			if (_rpod != "_") then {
				_unit addPrimaryWeaponItem _rpod;
			};
		};
	};
	case 4: {
		private _rifle = ([["arifle_Mk20_F", "30Rnd_556x45_Stanag"],
		["arifle_Mk20C_F", "30Rnd_556x45_Stanag"],
		["arifle_SPAR_01_khk_F", "30Rnd_556x45_Stanag"],
		["arifle_SPAR_02_khk_F", "30Rnd_556x45_Stanag"],
		["arifle_TRG20_F", "30Rnd_556x45_Stanag"],
		["arifle_TRG21_F", "30Rnd_556x45_Stanag"],
		["arifle_AK12_F", "30rnd_762x39_AK12_Lush_Mag_F"],
		["arifle_AK12U_F", "30rnd_762x39_AK12_Lush_Mag_F"]] call fnc_getRandom);
		private _rmag = _rifle select 1;
		_rifle = _rifle select 0;

		removeAllWeapons _unit;
		removeAllItems _unit;
		removeAllAssignedItems _unit;
		removeUniform _unit;
		removeVest _unit;
		removeBackpack _unit;
		removeHeadgear _unit;

		_unit addWeapon _rifle;
		_unit addPrimaryWeaponItem "acc_flashlight";
		_unit addPrimaryWeaponItem _rmag;

		_unit forceAddUniform (["U_BG_Guerilla1_1", "U_BG_Guerilla1_2_F", "U_BG_Guerilla_6_1",
			"U_BG_Guerilla2_1", "U_BG_Guerilla2_2", "U_BG_Guerilla2_3",
			"U_BG_leader", "U_I_C_Soldier_Camo_F", "U_I_C_Soldier_Para_1_F",
			"U_I_C_Soldier_Para_2_F", "U_I_C_Soldier_Para_3_F", "U_I_C_Soldier_Para_4_F"] call fnc_getRandom);
		_unit addVest (["V_Chestrig_oli", "V_Chestrig_khk", "V_Chestrig_blk",
			"V_PlateCarrierIA1_dgtl", "V_BandollierB_khk", "V_BandollierB_oli",
			"V_TacVest_blk", "V_TacVest_khk", "V_TacVest_oli",
			"V_TacVest_brn", "V_TacVest_camo", "V_I_G_resistanceLeader_F",
			"V_LegStrapBag_black_F", "V_LegStrapBag_coyote_F", "V_LegStrapBag_olive_F"] call fnc_getRandom);
		_unit addHeadgear (["H_Shemag_olive_hs", "H_Shemag_olive", "H_Shemag_tan",
			"H_MilCap_dgtl", "H_Helmet_Skate", "H_Cap_blk_Raven",
			"H_Booniehat_khk_hs", "H_Booniehat_oli", "H_Watchcap_khk",
			"H_Bandanna_mcamo", "H_Bandanna_camo", "H_HelmetIA"] call fnc_getRandom);

		_unit addItemToUniform "FirstAidKit";
		_unit addItemToUniform _rmag;
		_unit addItemToUniform "MiniGrenade";
		for "_i" from 1 to 6 do {_unit addItemToVest _rmag;};
		_unit addItemToVest "HandGrenade";
		_unit addItemToVest "SmokeShell";
		_unit addItemToVest "SmokeShellGreen";
		for "_i" from 1 to 2 do {_unit addItemToVest "Chemlight_blue";};

		_unit linkItem "ItemMap";
		_unit linkItem "ItemCompass";
		_unit linkItem "ItemWatch";
		_unit linkItem "ItemRadio";

		if (random 1 <= 0.2) then {
			_unit addBackpack (["B_FieldPack_oli", "B_FieldPack_cbr"] call fnc_getRandom);
			_unit addItemToBackpack "Medikit";
			for "_i" from 1 to 8 do { _unit addItemToBackpack "FirstAidKit"; };
			for "_i" from 1 to 7 do {_unit addItemToBackpack _rmag;};
			_unit setUnitTrait ["medic",true];
		};
		_unit enableGunLights 'ForceOn';
	};
	case 5: {
		private _rifle = ([["hgun_PDW2000_F", "30Rnd_9x21_Mag"],
			["SMG_02_F", "30Rnd_9x21_Mag"],
			["SMG_01_F", "30Rnd_45ACP_Mag_SMG_01"],
			["SMG_03C_black", "50Rnd_570x28_SMG_03"],
			["SMG_05_F", "30Rnd_9x21_Mag_SMG_02"],
			["sgun_HunterShotgun_01_sawedoff_F", "2Rnd_12Gauge_Pellets"],
			["sgun_HunterShotgun_01_F", "2Rnd_12Gauge_Pellets"],
			["srifle_DMR_06_hunter_F", "10Rnd_Mk14_762x51_Mag"]] call fnc_getRandom);
		private _rmag = _rifle select 1;
		_rifle = _rifle select 0;

		removeAllWeapons _unit;
		removeAllItems _unit;
		removeAllAssignedItems _unit;
		removeUniform _unit;
		removeVest _unit;
		removeBackpack _unit;
		removeHeadgear _unit;
		removeGoggles _unit;

		_unit addWeapon _rifle;
		_unit addPrimaryWeaponItem _rmag;

		_unit forceAddUniform (["U_O_R_Gorka_01_black_F", "U_C_E_LooterJacket_01_F",
			"U_I_L_Uniform_01_tshirt_black_F", "U_I_L_Uniform_01_tshirt_olive_F",
			"U_I_L_Uniform_01_tshirt_skull_F", "U_I_C_Soldier_Bandit_2_F"] call fnc_getRandom);
		_unit addBackpack (["B_LegStrapBag_black_F", "B_Messenger_Black_F"] call fnc_getRandom);

		_unit addItemToUniform "FirstAidKit";
		_unit addItemToUniform "ACE_Cellphone";
		for "_i" from 1 to 3 do {_unit addItemToUniform _rmag;};
		for "_i" from 1 to 2 do {_unit addItemToBackpack "FirstAidKit";};
		for "_i" from 1 to 6 do {_unit addItemToBackpack _rmag;};
		_unit addHeadgear (["H_Bandanna_gry", "H_Cap_blk", "H_Cap_blu",
				"H_Cap_blk_CMMG", "H_Cap_grn", "H_WirelessEarpiece_F",
				"H_Watchcap_blk", "H_Booniehat_taiga", "H_MilCap_grn",
				"H_Helmet_Skate", "H_cap_brn_SPECOPS", "H_MilCap_blue"] call fnc_getRandom);
		_unit addGoggles (["G_Bandanna_aviator", "G_Bandanna_beast", "G_Bandanna_blk",
			"G_Bandanna_khk", "G_Bandanna_oli", "G_Bandanna_shades",
			"G_Bandanna_sport", "G_Bandanna_tan"] call fnc_getRandom);

		_unit linkItem "ItemMap";
		_unit linkItem "ItemCompass";
		_unit linkItem "ItemWatch";
		_unit linkItem "ItemRadio";
	};
	default {};
};
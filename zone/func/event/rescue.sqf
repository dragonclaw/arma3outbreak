// Spawns a group of civilians for rescue.
// Usage [_zone] spawn fnc_zone_event_rescue;
// Returns: Nada.
_this spawn {
	params ["_zone"];

	private _fromPos = [random (_zone getVariable "areasize"), random 360, 90, (_zone getVariable "position")] call fnc_polToCart;
	private _newloc = [];
	private _pos = _fromPos;
	private _j = 0;
	while { count _newloc < 1 && _j < 99 } do {
		_newloc = [_pos, 0, 200, 5, 0, 0, 0] call BIS_fnc_findSafePos;
		_j = _j + 1;
	};
	if (count _newloc > 1) then { _pos = _newloc; };
	private _task = zone_group createUnit ["Logic", [0,0,0], [], 0, "NONE"];
	private _taskName = format["rescue_%1", (ceil (random 999999))];
	_task setVariable ["name", _taskName, false];
	private _units = [];
	_task setVariable ["units", _units, false];
	_task setVariable ["value", (ceil (random 201)) + 99];
	private _group = createGroup side_players;
	for "_i" from 0 to ((random (zone_group_size_limit / 2)) + (zone_group_size_limit / 2)) do {
		private _unit = _group createUnit [(zone_faction_units select zone_faction_survivor) call fnc_getRandom, _fromPos, [], 0, "NONE"];
		_unit setSkill random 1;
		[_unit, zone_faction_survivor] spawn fnc_zone_gearUnit;
		_unit setVariable ["task", _task, false];
		_units pushBackUnique _unit;
		[_unit, zone_action_group_rescue] call fnc_addActionGlobal;
		[_unit, 0, ["ACE_MainActions"], zone_action_group_save]
			call fnc_addAceActionToObjectGlobal;
		_unit addEventHandler ["Killed", {
			if (isServer) then {
				params ["_unit", "_killer", "_instigator", "_useEffects"];
				_unit spawn {
					sleep 120;
					deleteVehicle _this;
				};
				private _task = (_unit getVariable "task");
				if ({ alive _x } count (_task getVariable "units") <= 0) then {
					if (_task getVariable ["saved", 0] > 0) then {
						[(_task getVariable "name"),"SUCCEEDED"] call BIS_fnc_taskSetState;
					} else {
						[(_task getVariable "name"),"FAILED"] call BIS_fnc_taskSetState;
					};
					[(_task getVariable "name"),objNull] call BIS_fnc_taskSetDestination;
				} else {
					if (_unit getVariable ["taskTrack", false]) then {
						private _newunit = ((_task getVariable "units") call fnc_getRandom);
						[(_task getVariable "name"),[_newunit, true]] call BIS_fnc_taskSetDestination;
						_newunit setVariable ["taskTrack", true, false];
						_unit setVariable ["taskTrack", false, false];
					};
				};
			};
		}];
		_unit addEventHandler ["Deleted", {
			if (isServer) then {
				params ["_unit"];
				private _task = (_unit getVariable "task");
				private _units = (_task getVariable "units");
				if ({ alive _x } count (_task getVariable "units") <= 0) then {
					if (_task in zone_events) then {
						zone_events deleteAt (zone_events find _task);
					};
					deleteVehicle _task;
				};
				if (_unit in _units) then {
					_units deleteAt (_units find _unit);
				};
			};
		}];
		_unit spawn {
			waitUntil { (isNull _this) || (_this distance spawnPosition) < 50 };
			if (!(isNull _this)) then {
				private _task = (_this getVariable "task");
				private _units = (_task getVariable "units");
				_task setVariable ["saved", (_task getVariable ["saved", 0]) + 1, false];
				[(_task getVariable "value")] call fnc_base_earncash;
				if ({ alive _x } count _units <= 1) then {
					[(_task getVariable "name"),"SUCCEEDED"] call BIS_fnc_taskSetState;
					[(_task getVariable "name"),objNull] call BIS_fnc_taskSetDestination;
				};
				if (_this in _units) then {
					_units deleteAt (_units find _this);
				};
				doGetOut _this;
				unassignVehicle _this;
				sleep 4;
				deleteVehicle _this;
			};
		};
	};
	zone_events pushBackUnique _task;

	private _trackunit = ((_task getVariable "units") call fnc_getRandom);
	[side_players, _taskName, [format["Save the survivors by bringing them back to your Headquarters. Total value: $%1", (_task getVariable "value") * (count _units)], format["Rescue %1 Survivors", count _units], "Survivors"], [_trackunit, true], "AUTOASSIGNED", 2, true, "run", false] call BIS_fnc_taskCreate;
	_trackunit setVariable ["taskTrack", true, false];

	for "_i" from 0 to count allCurators -1 do {
		(allCurators select _i) addCuratorEditableObjects [_units,true];
	};
};
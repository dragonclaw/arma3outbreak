// Spawns a high value target.
// Usage [_zone] spawn fnc_zone_event_hvt;
// Returns: Nada.
_this spawn {
	params ["_zone"];

	private _task = zone_group createUnit ["Logic", [0,0,0], [], 0, "NONE"];
	private _taskName = format["hvt_%1", (ceil (random 999999))];
	_task setVariable ["name", _taskName, false];
	_task setVariable ["value", (ceil (random 2001)) + 499];
	_task setVariable ["zone", _zone];
	_task setVariable ["activate", {
		params ["_task"];
		private _zone = _task getVariable "zone";
		private _fromPos = [random (_zone getVariable "areasize"), random 360, 90, (_zone getVariable "position")] call fnc_polToCart;
		private _newloc = [];
		private _pos = _fromPos;
		private _j = 0;
		while { count _newloc < 1 && _j < 99 } do {
			_newloc = [_pos, 0, 200, 5, 0, 0, 0] call BIS_fnc_findSafePos;
			_j = _j + 1;
		};
		if (count _newloc > 1) then { _pos = _newloc; };
		private _group = createGroup side_infected;
		private _unit = _group createUnit [zone_faction_demons call fnc_getRandom, _fromPos, [], 0, "NONE"];
		_unit setSkill 1;
		_unit setVariable ["task", _task, false];
		_task setVariable ["unit", _unit, false];
		_unit addEventHandler ["Killed", {
			if (isServer) then {
				params ["_unit", "_killer", "_instigator", "_useEffects"];
				_unit spawn {
					sleep 120;
					deleteVehicle _this;
				};
				private _task = (_unit getVariable "task");
				[(_task getVariable "name"),"SUCCEEDED"] call BIS_fnc_taskSetState;
				[(_task getVariable "name"), [objNull, true]] call BIS_fnc_taskSetDestination;
				[(_task getVariable "value")] call fnc_base_earncash;
				private _zone_events = ((_task getVariable "zone") getVariable "events");
				if (_task in _zone_events) then {
					_zone_events deleteAt (_zone_events find _task);
				};
				deleteVehicle _task;
			};
		}];
		_group deleteGroupWhenEmpty true;
		[(_task getVariable "name"), [_unit, true]] call BIS_fnc_taskSetDestination;
		for "_i" from 0 to count allCurators -1 do {
			(allCurators select _i) addCuratorEditableObjects [[_unit],true];
		};
	}];
	_task setVariable ["deactivate", {
		params ["_task"];
		private _zone = _task getVariable "zone";
		if ((_zone getVariable "infected") < 1 && (_zone getVariable "living") < 1) then {
			(_task getVariable "unit") addEventHandler ["Deleted", {
				if (isServer) then {
					params ["_unit"];
					private _taskName = ((_unit getVariable "task") getVariable "name");
					if (_taskName call BIS_fnc_taskState != "SUCCEEDED") then {
						[_takeName,"CANCELED"] call BIS_fnc_taskSetState;
					};
				};
			}];
		} else {
			deleteVehicle (_task getVariable ["unit", objNull]);
			_task setVariable ["unit", objNull];
			[(_task getVariable "name"), getPos (_task getVariable "zone")] call BIS_fnc_taskSetDestination;
		};
	}];
	private _zone_events = (_zone getVariable "events");
	_zone_events pushBackUnique _task;

	[side_players, _taskName, [format["Take out this high value target. Value: $%1", (_task getVariable "value")], "Kill Target", "Kill"], getPos _zone, "AUTOASSIGNED", 2, true, "kill", false] call BIS_fnc_taskCreate;
};
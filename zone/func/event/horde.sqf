// Spreads infection from zone to zone.
// Usage [_from, _target] spawn fnc_zone_horde;
// Returns: Nada.
_this spawn {
	params ["_from", "_target"];
	sleep (random 120);

	private _fromPos = [random (_from getVariable "areasize"), random 360, 90, (_from getVariable "position")] call fnc_polToCart;
	private _newloc = [];
	private _pos = _fromPos;
	private _j = 0;
	while { count _newloc < 1 && _j < 99 } do {
		_newloc = [_pos, 0, 200, 5, 0, 0, 0] call BIS_fnc_findSafePos;
		_j = _j + 1;
	};
	if (count _newloc > 1) then { _pos = _newloc; };
	private _group = createGroup side_infected;
	for "_i" from 0 to ((random (zone_group_size_limit / 2)) + (zone_group_size_limit / 2)) do {
		private _unit = _group createUnit [(zone_faction_units select zone_faction_infected) call fnc_getRandom, _fromPos, [], 0, "NONE"];
		_unit setSkill random 1;
		_unit setVariable [];
		_unit addEventHandler ["Killed", {
			if (isServer) then {
				params ["_unit", "_killer", "_instigator", "_useEffects"];
				_unit spawn {
					sleep 120;
					deleteVehicle _this;
				};
				private _task = ((group _unit) getVariable "task");
				if ({alive _x} count (units (group _unit)) <= 0) then {
					
					[(_task getVariable "name"),"SUCCEEDED"] call BIS_fnc_taskSetState;
					[(_task getVariable "name"),objNull] call BIS_fnc_taskSetDestination;
				} else {
					if (_unit getVariable ["taskTrack", false]) then {
						private _newunit = ((_task getVariable "units") call fnc_getRandom);
						[(_task getVariable "name"),[_newunit, true]] call BIS_fnc_taskSetDestination;
						_newunit setVariable ["taskTrack", true, false];
						_unit setVariable ["taskTrack", false, false];
					};
				};
				[(_task getVariable "value")] call fnc_base_earncash;
			};
		}];
		_unit addEventHandler ["Deleted", {
			if (isServer) then {
				params ["_unit"];
				if ({alive _x} count (units (group _unit)) <= 0) then {
					private _group = (group _unit);
					private _task = (_group getVariable "task");
					if (_task in zone_events) then {
						zone_events deleteAt (zone_events find _task);
					};
					if (_group in zone_hordes) then {
						zone_hordes deleteAt (zone_hordes find _group);
					};
					deleteVehicle _task;
					deleteGroup _group;
				};
			};
		}];
		[_unit] spawn {
			params ["_unit"];
			private _task = ((group _unit) getVariable "task");
			waitUntil { (isNull _unit) || (_unit distance _task) < 50 };
			if (!(isNull _unit)) then {
				private _units = (units (group _unit));
				if ({ alive _x } count _units <= 1) then {
					[(_task getVariable "name"),"FAILED"] call BIS_fnc_taskSetState;
					[(_task getVariable "name"),objNull] call BIS_fnc_taskSetDestination;
				};
				deleteVehicle _unit;
			};
		};
		[_unit] spawn {
			params ["_unit"];
			private _task = ((group _unit) getVariable "task");
			while { !(isNull _unit) && alive _unit } do {
				if(isNull (_unit getVariable ["RZ_Target", objNull])) then {
					[_unit,format["(zone_hordes select %1) getVariable ""task""", (zone_hordes find (group _unit))]] call RZ_fnc_zombie_gotoWaypoint;
				};
				uisleep 10;
			};
		};
	};
	_group setVariable ["target", _target, false];
	_group deleteGroupWhenEmpty true;
	private _targPos = [(_target getVariable "position"), 0, 200, 5, 0, 0, 0] call BIS_fnc_findSafePos;
	private _task = zone_group createUnit ["Logic", _targPos, [], 0, "NONE"];
	_group setVariable ["task", _task, false];
	private _taskName = format["horde_%1", (ceil (random 999999))];
	_task setVariable ["name", _taskName, false];
	_task setVariable ["value", (ceil (random 351)) + 49];
	zone_hordes pushBack _group;
	zone_events pushBackUnique _task;
	private _trackunit = ((units _group) call fnc_getRandom);
	_trackunit setVariable ["taskTrack", true, false];
	[side_players, _taskName, [format["Destroy the horde. Total value: $%1", (_task getVariable "value") * (count (units _group))], format["Kill %1 Zombies", (count (units _group))], "Horde"], [_trackunit, true], "AUTOASSIGNED", 2, true, "meet", false] call BIS_fnc_taskCreate;
	for "_i" from 0 to count allCurators -1 do {
		(allCurators select _i) addCuratorEditableObjects [(units _group),TRUE];
		(allCurators select _i) addCuratorEditableObjects [_task,TRUE];
	};
};
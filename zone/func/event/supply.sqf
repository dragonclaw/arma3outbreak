// Spawns a group of civilians for rescue.
// Usage [_zone] spawn fnc_zone_event_rescue;
// Returns: Nada.
_this spawn {
	params ["_zone"];

	private _fromPos = [random (_zone getVariable "areasize"), random 360, 90, (_zone getVariable "position")] call fnc_polToCart;
	private _newloc = [];
	private _pos = _fromPos;
	private _j = 0;
	while { count _newloc < 1 && _j < 99 } do {
		_newloc = [_pos, 0, 200, 5, 0, 0, 0] call BIS_fnc_findSafePos;
		_j = _j + 1;
	};
	if (count _newloc > 1) then { _pos = _newloc; };
	private _task = zone_group createUnit ["Logic", [0,0,0], [], 0, "NONE"];
	private _taskName = format["rescue_%1", (ceil (random 999999))];
	_task setVariable ["name", _taskName, false];
	_task setVariable ["value", (ceil (random 1001)) + 499];
	private _group = createGroup side_players;
	private _vtype = ["B_Truck_01_box_F", "C_Truck_02_covered_F", "C_Offroad_01_repair_F", "C_Van_01_box_F"] call fnc_getRandom;
	private _vehicle = _vtype createVehicle _pos;
	if (_vtype == "B_Truck_01_box_F") then {
		_vehicle setObjectTextureGlobal [0,"\dc\bstf\data\dc_truck_01_ext_01_bstf_co.paa"];
		_vehicle setObjectTextureGlobal [1,"\dc\bstf\data\dc_truck_01_ext_02_bstf_co.paa"];
		_vehicle setObjectTextureGlobal [2,"\dc\bstf\data\dc_truck_01_ammo_bstf_co.paa"];
	};
	_vehicle setVariable ["task", _task, false];
	_vehicle addEventHandler ["Killed", {
		if (isServer) then {
			params ["_vehicle", "_killer", "_instigator", "_useEffects"];
			_vehicle spawn {
				sleep 120;
				deleteVehicle _this;
			};
			private _task = (_vehicle getVariable "task");
			if ([(_task getVariable "name")] call BIS_fnc_taskState != "SUCCEEDED") then {
				[(_task getVariable "name"),"FAILED"] call BIS_fnc_taskSetState;
				[(_task getVariable "name"),objNull] call BIS_fnc_taskSetDestination;
			};
			if (_task in zone_events) then {
				zone_events deleteAt (zone_events find _task);
			};
			deleteVehicle _task;
		};
	}];
	_vehicle spawn {
		waitUntil { (isNull _this) || (_this distance spawnPosition) < 50 };
		if (!(isNull _this)) then {
			private _task = (_this getVariable "task");
			[(_task getVariable "value")] call fnc_base_earncash;
			[(_task getVariable "name"),"SUCCEEDED"] call BIS_fnc_taskSetState;
			[(_task getVariable "name"),objNull] call BIS_fnc_taskSetDestination;
			if (_task in zone_events) then {
				zone_events deleteAt (zone_events find _task);
			};
			deleteVehicle _task;
			_this setFuel 0;
			sleep 30;
			deleteVehicle _this;
		};
	};
	zone_events pushBackUnique _task;

	[side_players, _taskName, [format["Recover the supplies by bringing them back to your Headquarters. Value: $%1", (_task getVariable "value")], "Recover Supplies", "Supplies"], [_vehicle, true], "AUTOASSIGNED", 2, true, "box", false] call BIS_fnc_taskCreate;

	for "_i" from 0 to count allCurators -1 do {
		(allCurators select _i) addCuratorEditableObjects [[_vehicle],true];
	};
};
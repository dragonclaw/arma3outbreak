if (!isServer) exitWith {};
params ["_zonePositions"];
fnc_zone_spawnGroup = compileFinal preprocessFile "zone\func\spawnGroup.sqf";
fnc_zone_spawnUnit = compileFinal preprocessFile "zone\func\spawnUnit.sqf";
fnc_zone_populate = compilefinal preprocessFile "zone\func\populate.sqf";
fnc_zone_gearUnit = compilefinal preprocessFile "zone\func\gearUnit.sqf";
fnc_zone_spreader = compilefinal preprocessFile "zone\func\spreader.sqf";
fnc_zone_event_horde = compileFinal preprocessFile "zone\func\event\horde.sqf";
fnc_zone_event_hvt = compileFinal preprocessFile "zone\func\event\hvt.sqf";
fnc_zone_event_rescue = compilefinal preprocessFile "zone\func\event\rescue.sqf";
fnc_zone_event_supply = compileFinal preprocessFile "zone\func\event\supply.sqf";
private _typeScript = [] execVM "zone\type.sqf";
waitUntil { scriptDone _typeScript };

private _zonecount = count _zonePositions;
if (_zonecount < 1) exitWith {
	[Announcer, "CRITICAL MAP ERROR: THERE ARE NO ZONES."] remoteExec ["globalChat", 0];
	uisleep 30;
	["end3", true, true, true, true] remoteExec ["BIS_fnc_endMission"];
};
private _allhandles = [];
for "_i" from _zonecount -1 to 0 step -1 do {
	private _zoneinfo = (_zonePositions select _i);
	private _handle = [_zoneinfo select 0, _zoneinfo select 3, _zoneinfo select 1, _zoneinfo select 2] spawn fnc_zone_spawn;
	_allhandles pushBack _handle;
};
waitUntil { (_allhandles call fnc_allScriptsDone) };
private _infectzones = ceil (((_zonecount / 6)
			max (random _zonecount))
			min (_zonecount / 2));
private _allzones = zone_all call fnc_shuffle;
private _i = 0;

for "_i" from 0 to count zone_all -1 do {
	private _zone = _allzones select _i;
	if (_infectzones > 0) then {
		[_zone, ((_zone getVariable ["living", 0]) / 100)
			max (random ((_zone getVariable ["living", 0]) min 100))] call fnc_zone_infectliving;
		// Free spreader, stay on your toes!!
		_zone spawn {
			sleep (300 + (random 900));
			[_this, ((zone_all - [_this]) call fnc_getRandom)]
				call fnc_zone_spreader;
		};
		_infectzones = _infectzones - 1;
	};
};
publicVariable "zone_all"; // Hack to make interactions work !!FIX ME!!
publicVariable "zone_total_population"; // This too!!

[] execVM "zone\manager.sqf";

private _boxes = allMissionObjects "ReammoBox_F";
for "_i" from count _boxes -1 to 0 step -1 do {
	private _box = (_boxes select _i);
	clearWeaponCargoGlobal _box;
	clearMagazineCargoGlobal _box;
	clearItemCargoGlobal _box;
	[_box, true] call ace_arsenal_fnc_initBox;
};

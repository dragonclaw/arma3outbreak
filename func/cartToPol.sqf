// 3d Cartesean to Polar Coordinate translation function.
// Rho donates the distance from center.
// Theta is the direction on the x/y plane. (Longtitude)
// Phi is the direction on the z plane. (Lattitude)
// USAGE: [_x, _y, _z (optional)] call fnc_cartToPol;
// RETURNS: [_rho, _theta, _phi]
private ["_x", "_y", "_z", "_rho", "_phi"];
_x = (_this select 0);
_y = (_this select 1);
_z = 0;
_rho = 0;
_phi = 90;
if (count _this > 2) then {
	_z = (_this select 2);
	_rho = (sqrt ((_x * _x) + (_y * _y) + (_z * _z)));
	if (_rho != 0) then { _phi = acos (_z / _rho); };
} else {
	_rho = (sqrt ((_x * _x) + (_y * _y)));
};

[_rho, (_x atan2 _y), _phi]

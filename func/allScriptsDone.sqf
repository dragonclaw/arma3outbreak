// Returns true if an array of script handles have finished.
// Usage: [scriptHandle, scriptHandle, ...] call fnc_allScriptsDone.sqf;
// Returns: BOOL
private _continue = true;
for "_i" from 0 to (count _this) -1 do {
	if (!scriptDone (_this select _i)) then { _continue = false; };
};
_continue
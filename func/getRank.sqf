private ["_text"];
_text = "PRIVATE";
if (typename _this == "SCALAR") then {
	switch (true) do {
		case (_this >= 7500): { _text = "COLONEL"; };
		case (_this < 7500 && _this >= 5000): { _text = "MAJOR"; };
		case (_this < 5000 && _this >= 3500): { _text = "CAPTAIN"; };
		case (_this < 3500 && _this >= 2500): { _text = "LIEUTENANT"; };
		case (_this < 2500 && _this >= 1500): { _text = "SERGEANT"; };
		case (_this < 1500 && _this >= 500): { _text = "CORPORAL"; };
		default {};
	};
};
_text
// Function to find if anyone important is stood near a position.
// USAGE: [[_x, _y, _z] (or Object), _additionalDistance (optional)] call fnc_inZone;
// RETURNS: true/false
params ["_position", ["_additional", 0]];
private _return = objNull;
for "_i" from (count zone_all) -1 to 0 step -1 do {
	private _zone = (zone_all select _i);
	if ((_zone distance _position) < (_zone getVariable ["areasize", 0]) + _additional) then {
		_return = _zone;
		break;
	};
};

_return

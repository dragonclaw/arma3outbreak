(_this select 0) setOvercast (_this select 1);
if (isServer) then {
	private _fog = (_this select 2);
	if (_fog < 0) then { _fog = -5000; };
	(_this select 0) setFog [(_this select 1), 1, _fog];
};
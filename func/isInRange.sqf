// Function to find if anyone important is stood near a position.
// USAGE: [[_x, _y, _z], _range, [_unit] (optional)] call fnc_isInRange;
// RETURNS: true/false
params ["_position", "_range", ["_units", playableUnits + switchableUnits]];
private _return = false;
for "_i" from (count _units) -1 to 0 step -1 do {
	if (((_units select _i) distance _position) < _range) then {
		_return = true;
		break;
	};
};

_return

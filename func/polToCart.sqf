// 3d Cartesean to Polar Coordinate translation function.
// Rho donates the distance from center.
// Theta is the direction on the x/y plane. (Longtitude)
// Phi is the direction on the z plane. (Lattitude)
// Extra Cartesean coordinates are the global coordinate for our local zero point.
// USAGE: [_rho, _theta, _phi (optional), [_x, _y, _z] (optional)] call fnc_polToCart;
// RETURNS: [_x, _y, _z]
private ["_rho", "_theta", "_phi", "_x", "_y", "_z"];
_rho = _this select 0;
_theta = _this select 1;
_phi = 90;
_x = 0;
_y = 0;
_z = 0;
if (count _this > 2) then {
	_phi = _this select 2;
};
if (count _this > 3 && typeName (_this select 3) == "ARRAY") then {
	_x = (_this select 3) select 0;
	_y = (_this select 3) select 1;
	if (count (_this select 3) > 2) then {
		_z = (_this select 3) select 2;
	};
};

[(_rho * (sin _phi) * (sin _theta)) + _x, (_rho * (sin _phi) * (cos _theta)) + _y, (_rho * (cos _phi)) + _z]

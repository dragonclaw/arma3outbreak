private ["_baseX", "_baseY", "_baseZ", "_offX", "_offY", "_offZ"];
_baseX = (_this select 0) select 0;
_baseY = (_this select 0) select 1;
_baseZ = (_this select 0) select 2;
_offX = (_this select 1) select 0;
_offY = (_this select 1) select 1;
_offZ = (_this select 1) select 2;

[(_offX - _baseX), (_offY - _baseY), (_offZ - _baseZ)]
if (!ace_available) exitWith {};
private _targets = 0;
if (isDedicated) then { _targets = -2; };

private _jip = _this remoteExec ["ace_interact_menu_fnc_addActionToObject", _targets, true];
private _jipids = (_this select 0) getVariable ["jipids_aceact", []];
if (count _jipids < 1) then {
	(_this select 0) setVariable ["jipids_aceact", _jipids, false];
	(_this select 0) addEventHandler ["Deleted", {
		params ["_unit"];
		private _jipids = _unit getVariable ["jipids_aceact", []];
		if (count _jipids > 0) then {
			for "_j" from 0 to (count _jipids) -1 do {
				remoteExec ["", 0, _jipids select _j];
			};
		};
	}];
};
_jipids pushBackUnique _jip;
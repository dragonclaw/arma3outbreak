
/*
kp_fuel_consumption.sqf
Author: Wyqer
Website: https://www.killahpotatoes.de
Source: https://github.com/Wyqer/A3-Scripts
Date: 2017-02-02
License:
MIT License

Copyright (c) 2017 Wyqer

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


Description:
This script handles the fuel consumption of vehicles, so that refueling will be necessary more often.

Parameters:
_this select 0 - OBJECT - Vehicle

Method:
execVM

Example for initPlayerLocal.sqf:
player addEventHandler ["GetInMan", {[_this select 2] execVM "scripts\kp_fuel_consumption.sqf";}];
*/

private ["_kp_neutral_consumption","_kp_normal_consumption","_kp_max_consumption", "_vehicle"];

// CONFIG START

// Time in Minutes till a full tank depletes when the vehicle is standing with running engine
_kp_neutral_consumption = 90;
// Time in Minutes till a full tank depletes when the vehicle is driving
_kp_normal_consumption = 30;
// Time in Minutes till a full tank depletes when the vehicle is driving at max speed
_kp_max_consumption = 20;

// CONFIG END

// DO NOT EDIT BELOW
if (isDedicated) exitWith {};
if (isNil "kp_fuel_consumption_vehicles") then {
	kp_fuel_consumption_vehicles = [];
};
_vehicle = _this select 0;
if (_vehicle != (vehicle player)) exitWith {};

if !(_vehicle in kp_fuel_consumption_vehicles) then {
	kp_fuel_consumption_vehicles pushBack _vehicle;
	while {local _vehicle} do {
		if (isEngineOn _vehicle) then {
			if (speed _vehicle > 5) then {
				if (speed _vehicle > (getNumber (configFile >> "CfgVehicles" >> typeOf _vehicle >> "maxSpeed") * 0.9)) then {
					_vehicle setFuel (fuel _vehicle - (1 / (_kp_max_consumption * 60)));
				} else {
					_vehicle setFuel (fuel _vehicle - (1 / (_kp_normal_consumption * 60)));
				};
			} else {
				_vehicle setFuel (fuel _vehicle - (1 / (_kp_neutral_consumption * 60)));
			};
		};
		uiSleep 1;
	};
	kp_fuel_consumption_vehicles deleteAt (kp_fuel_consumption_vehicles find _vehicle);
};

if (!hasInterface) exitWith {};
//params ["_global_infected", "_global_surviving"];
with uiNamespace do {
	private _global_population = missionNamespace getVariable ["zone_total_population", 0];
	private _global_infected = 0;
	private _global_surviving = 0;
	private _zone_all = missionNamespace getVariable ["zone_all", []];
	private _name = "Island";
	if (isNull ui_zone) then {
		for "_i" from 0 to (count _zone_all) -1 do {
			private _zone = (_zone_all select _i);
			_global_infected = _global_infected + (_zone getVariable ["infected", 0]);
			_global_surviving = _global_surviving + (_zone getVariable ["living", 0]);

			if (player distance _zone <= (_zone getVariable ["areasize", -301]) + 300) then {
				ui_zone = _zone;
				break;
			};
		};
	} else {
		_name = ui_zone getVariable ["name", "Somewhere"];
		_global_population = ui_zone getVariable ["Population", 0];
		_global_infected = ui_zone getVariable ["infected", 0];
		_global_surviving = ui_zone getVariable ["living", 0];

		if (player distance ui_zone > (ui_zone getVariable ["areasize", -301]) + 300) then {
			ui_zone = objNull;
		};
	};
	CashHudText ctrlSetStructuredText (parseText format ["%5 Population: <t color='#AAAAAA'>%2</t>%1Survivors: <t color='#4400FF'>%3</t>%1Infected: <t color='#FF4444'>%4</t>%1Cash: <t color='#448844'>$%6</t>", "&#160;&#160;&#160;&#160;|&#160;&#160;&#160;&#160;", _global_population, _global_surviving, _global_infected, _name, (missionNamespace getVariable ["base_cash", 0])]);
	CashHudText ctrlCommit 0;
};

// Kill hud: _id cutText ["", "PLAIN"];

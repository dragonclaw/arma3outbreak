// Global functions!
KK_fnc_setPosAGLS = compileFinal preprocessFile "func\KK_fnc_setPosAGLS.sqf";
fnc_getRandom = compileFinal preprocessFile "func\getRandom.sqf";
fnc_shuffle = compileFinal preprocessFile "func\shuffle.sqf";
fnc_getRank = compileFinal preprocessFile "func\getRank.sqf";
fnc_polToCart = compileFinal preprocessFile "func\polToCart.sqf";
fnc_cartToPol = compileFinal preprocessFile "func\cartToPol.sqf";
fnc_cartConcat = compileFinal preprocessFile "func\cartConcat.sqf";
fnc_cartSplit = compileFinal preprocessFile "func\cartSplit.sqf";
fnc_allScriptsDone = compileFinal preprocessFile "func\allScriptsDone.sqf";
fnc_setWeather = compileFinal preprocessFile "func\setWeather.sqf";
fnc_isInRange = compileFinal preprocessFile "func\isInRange.sqf";
fnc_inZone = compileFinal preprocessFile "func\inZone.sqf";
fnc_updateCashHud = compileFinal preprocessFile "func\updateCashHud.sqf";
fnc_addActionGlobal = compileFinal preprocessFile "func\addActionGlobal.sqf";
fnc_addAceActionToObjectGlobal = compileFinal preprocessFile "func\addAceActionToObjectGlobal.sqf";
ace_available = (isClass(configFile >> "CfgPatches" >> "ace_main"));
bstf_available = (isClass(configFile >> "CfgPatches" >> "dc_bstf"));

[] execVM "loudspeaker\ls_init.sqf";

// Empty loadout for stripping characters.
EmptyPlayerLoadout = [["", "", "", "", ["", 0], [], ""], [],
	["", "", "", "", ["", 0], [], ""],
	["", []],
	["", []],
	[], "", "", [], ["","","","","",""]];

if (bstf_available) then {
	bstfLoadout = [["arifle_MXC_Black_F","muzzle_snds_H","acc_flashlight","optic_Aco",["30Rnd_65x39_caseless_black_mag",30],[],""],[],
		["hgun_P07_F","muzzle_snds_l","","",["16Rnd_9x21_Mag",17],[],""],
		["dc_U_bstf_CombatUniform",[["FirstAidKit",1],["Chemlight_blue",1,1],["30Rnd_65x39_caseless_black_mag_Tracer",2,30]]],
		["dc_V_PlateCarrier1_bstf",[["16Rnd_9x21_Mag",2,17],["SmokeShell",1,1],["SmokeShellBlue",1,1],["Chemlight_blue",1,1],["HandGrenade",2,1],["30Rnd_65x39_caseless_black_mag",7,30]]],
		[],"dc_H_HelmetSpecB_bstf","",[],["ItemMap","","ItemRadio","ItemCompass","ItemWatch","NVGoggles_OPFOR"]];
};

if (ace_available && hasInterface) then {
	private _cureaction = ["RyanZombie_CureInfected", "Cure Infection", "", {
				[2, [_player, _target], {
					private _player = (_this select 0) select 0;
					private _target = (_this select 0) select 1;
					if (!(("Medikit" in (items _player)) && (_player getUnitTrait "medic"))) then {
						_player removeItem "RyanZombiesAntiVirusCure_Item";
					};
					_target setVariable ["ryanzombiesinfected", 0, true];
					if (side _target == side _player) then {
						_target sideChat "Thank you!";
					} else {
						_target globalChat "Thank you!";
					};
				}, {}, "Curing..."] call ace_common_fnc_progressBar;
			},
			{ ((_target getVariable ["ryanzombiesinfected", 0]) > 0)
				&& ((("Medikit" in (items _player)) && (_player getUnitTrait "medic"))
				|| ("RyanZombiesAntiVirusCure_Item" in (items _player))) }, {}, [], [0,0,0], 2
		] call ace_interact_menu_fnc_createAction;
	["Man", 0, ["ACE_MainActions"], _cureaction, true]
		call ace_interact_menu_fnc_addActionToClass;
};

setViewDistance 4000;
setObjectViewDistance 2000;

author = "Rufus";
onLoadName = "Outbreak";

respawn = 2;
respawnDelay = 10;
respawnOnStart = -1;
respawnTemplates[] = { "MenuPosition", "Tickets", "Spectator" };

reviveMode = 1;
reviveUnconsciousStateMode = 0;
reviveRequiredTrait = 0;
reviveRequiredItems = 0;
reviveRequiredItemsFakConsumed = 0;
reviveMedicSpeedMultiplier = 2;
reviveDelay = 12;
reviveForceRespawnDelay = 8;
reviveBleedOutDelay = 1200;

class CfgDebriefing
{  
	class End1
	{
		title = "Mission Completed";
		subtitle = "Critical Success";
		description = "You successfully purged the infection from the island!";
		pictureBackground = "";
		picture = "b_inf";
		pictureColor[] = { 0.0, 0.3, 0.6, 1 };
	};
	class End2
	{
		title = "Mission Failed";
		subtitle = "Defeat";
		description = "Your forces were overcome.";
		pictureBackground = "";
		picture = "KIA";
		pictureColor[] = { 0.6, 0.1, 0.2, 1 };
	};
	class End3
	{
		title = "Mission End";
		subtitle = "Closure";
		description = "You purged the infection. But at what cost?";
		pictureBackground = "";
		picture = "b_inf";
		pictureColor[] = { 0.6, 0.1, 0.2, 1 };
	};
	class End4
	{
		title = "Mission Failed";
		subtitle = "Defeat";
		description = "The helicopter was destroyed.";
		pictureBackground = "";
		picture = "KIA";
		pictureColor[] = { 0.6, 0.1, 0.2, 1 };
	};
};

class CfgDebriefingSections
{
	class EndPopulation
	{
		title = "Population Stats";
		variable = "end_score";
	};
	class EndFinance
	{
		title = "Finance Stats";
		variable = "end_cash";
	};
};

class RscTitles {
	class RscCashHud {
		idd = 4040;
		fadein = 0;
		fadeout = 0;
		duration = 1e+11;
		movingEnable = 0;
		enableSimulation = 1;
		onLoad = "uiNamespace setVariable ['CashHud', (_this select 0)];";
		onUnload = "uiNamespace setVariable ['CashHud', nil];";
		class Controls
		{
			class CashHud
			{
				idc = 4041;
				style = 0x00;
				font = "RobotoCondensedBold";
				x = safeZoneX + 0.3 * safeZoneW;
				y = safeZoneY + 0.004 * safeZoneH;
				w = safeZoneW * 0.4;
				h = safeZoneH * 0.03;
				colorText[] = {0.0,0.0,0.0,1};
				colorBackground[] = {0,0,0,0.3};
				text = "Please Wait...";
				lineSpacing = 1;
				size = 0.045;
				deletable = 0;
				fade = 0;
				access = 0;
				type = 13;
				shadow = 1;
				colorShadow[] = {0,0,0,0.5};
				class Attributes
				{
					font = "RobotoCondensed";
					color = "#ffffff";
					colorLink = "#D09B43";
					align = "center";
					valign = "top";
					shadow = 1;
				};
			};
		};
	};
};

// Used by say3D to determine file location.
// See loudspeaker\ls_init.sqf for the listed song definitions.
class CfgSounds
{
	sounds[] = {};
	// Ace Combat
	class ace7alicorn
	{
		// how the sound is referred to in the editor (e.g. trigger effects)
		name = "Ace7 Alicorn";

		// filename, volume, pitch, distance (optional)
		sound[] = { "@RomeMusic\Games\Alicorn-AceCombat7.ogg", 5, 1, 3000, 1, 0, 0, 0 };

		titles[] = {};
	};
	class ace7archange
	{
		name = "Ace7 Archange";
		sound[] = { "@RomeMusic\Games\Archange-AceCombat7.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};
	class ace7daredevil
	{
		name = "Ace7 Daredevil";
		sound[] = { "@RomeMusic\Games\Daredevil-AceCombat7.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};
	class ace5unsung
	{
		name = "Ace5 The Unsung War";
		sound[] = { "@RomeMusic\Games\TheUnsungWar.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};

	// ACDC
	class acdctnt
	{
		name = "ACDC - TNT";
		sound[] = { "@RomeMusic\ACDC\ACDC_TNT.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};
	class acdcback
	{
		name = "ACDC - Back In Black";
		sound[] = { "@RomeMusic\ACDC\BackInBlackLyrics.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};
	class acdchellsbells
	{
		name = "ACDC - Hells Bells";
		sound[] = { "@RomeMusic\ACDC\Hells_Bells.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};
	class acdchighway
	{
		name = "ACDC - Highway to Hell";
		sound[] = { "@RomeMusic\ACDC\HighwaytoHell.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};

	// Country
	class coaint
	{
		name = "Ain't I Right";
		sound[] = { "@RomeMusic\Country\AintIRight.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};
	class cobig
	{
		name = "Big Iron";
		sound[] = { "@RomeMusic\Country\BigIron.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};
	class coroad
	{
		name = "Take Me Home Country Roads";
		sound[] = { "@RomeMusic\Country\TakeMeHomeCountryRoads.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};
	class cohurt
	{
		name = "Johnny Cash - Hurt";
		sound[] = { "@RomeMusic\Country\JohnnyCash-Hurt.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};

	// Dark Synth
	class dsmob
	{
		name = "Curpenter Brut - Roller Mobster";
		sound[] = { "@RomeMusic\Darksynth\CarpenterBrut-RollerMobster.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};
	class dsbehemoth
	{
		name = "Perturbator - Behemoth";
		sound[] = { "@RomeMusic\Darksynth\Perturbator-Behemoth.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};
	class dscorrupt
	{
		name = "Perturbator - Corrupted By Design";
		sound[] = { "@RomeMusic\Darksynth\perturbator_corrupted_by_design.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};
	class dsdeath
	{
		name = "Perturbator - Death Squad";
		sound[] = { "@RomeMusic\Darksynth\Perturbator-DeathSquad.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};
	class dsdisco
	{
		name = "Perturbator - Disco Inferno";
		sound[] = { "@RomeMusic\Darksynth\Perturbator-DiscoInferno.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};
	class dshumans
	{
		name = "Perturbator - Humans Are Such Easy Prey";
		sound[] = { "@RomeMusic\Darksynth\Perturbator-HumansAreSuchEasyPrey.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};

	// Halo 3
	class halofinish
	{
		name = "Halo 3 - Finish The Fight";
		sound[] = { "@RomeMusic\Games\Halo3FinishTheFight.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};
	class halonever
	{
		name = "Halo 3 - Never Forget";
		sound[] = { "@RomeMusic\Games\Halo3NeverForget.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};
	class halofinal
	{
		name = "Halo 3 - One Final Effort";
		sound[] = { "@RomeMusic\Games\Halo3OneFinalEffort.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};
	class halocall
	{
		name = "Halo 3 - Roll Call";
		sound[] = { "@RomeMusic\Games\Halo3RollCall.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};

	// Movies
	class movie28days
	{
		name = "28 Days Later - In the House, In the Heartbeat";
		sound[] = { "@RomeMusic\Movies\28days.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};
	class movieabridgetoofar
	{
		name = "A Bridge Too Far - Main Theme";
		sound[] = { "@RomeMusic\Movies\A_Bridge_Too_Far-Theme.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};
	class movieapril1945
	{
		name = "Fury - April 1945";
		sound[] = { "@RomeMusic\Movies\April1945.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};
	class moviebandofbrothers
	{
		name = "Band of Brothers Intro";
		sound[] = { "@RomeMusic\Movies\Band_of_Brothers.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};
	class movieclubbed
	{
		name = "Matrix - Clubbed to Death";
		sound[] = { "@RomeMusic\Movies\clubbedtodeath.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};
	class movietopgun
	{
		name = "Top Gun - Anthem";
		sound[] = { "@RomeMusic\Movies\TopGunAnthem.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};
	class moviedangerzone
	{
		name = "Top Gun - Danger Zone";
		sound[] = { "@RomeMusic\Movies\TopGun-DangerZone.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};
	class movieafewdollars
	{
		name = "For A Few Dollars More";
		sound[] = { "@RomeMusic\Movies\ForAFewDollars.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};
	class moviegeralt
	{
		name = "The Witcher - Geralt Of Rivia";
		sound[] = { "@RomeMusic\Movies\GeraltOfRiviaTheWitcherOST.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};
	class movieagoodday
	{
		name = "Starship Troopers - It's A Good Day To Die";
		sound[] = { "@RomeMusic\Movies\StarshipTroopersItsAGoodDayToDie.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};
	class moviessttheme
	{
		name = "Starship Troopers Theme";
		sound[] = { "@RomeMusic\Movies\StarshipTroopersTheme.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};
	class moviegoodbadugly
	{
		name = "The Good, The Bad and The Ugly Theme";
		sound[] = { "@RomeMusic\Movies\TheGoodtheBadandtheUgly.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};
	class movietosser
	{
		name = "The Witcher - Toss A Coin To Your Witcher";
		sound[] = { "@RomeMusic\Movies\TossACoinToYourWitcher.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};
	class moviejojo
	{
		name = "Jojo - Il Vento D'oro";
		sound[] = { "@RomeMusic\Anime\jojo_il_vento_d'oro.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};
	class moviegladiator
	{
		name = "Gladiator - Now We Are Free";
		sound[] = { "@RomeMusic\Movies\NowWeAreFree.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};
	class movieevangelion
	{
		name = "Evangelion - Komm Susser Todd";
		sound[] = { "@RomeMusic\Anime\kommsussertod_m_10directorseditversion_.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};

	// Military
	class milussr
	{
		name = "Soviet National Anthem";
		sound[] = { "@RomeMusic\Military\NationalAnthemoftheUSSR.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};

	// Metal
	class metaldeadmen
	{
		name = "Attack of the Dead Men";
		sound[] = { "@RomeMusic\HeavyMetal\AttackOfTheDeadMen.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};
	class metalhateme
	{
		name = "Ded - Hate Me";
		sound[] = { "@RomeMusic\HeavyMetal\Ded-HateMe.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};
	class metalflanders
	{
		name = "Sabaton - In Flanders Fields";
		sound[] = { "@RomeMusic\HeavyMetal\InFlandersFields.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};

	// Rock
	class rockhail
	{
		name = "Avenged Sevenfold - Hail to the King";
		sound[] = { "@RomeMusic\RockClassic\avenged_sevenfold_hail_to_the_king.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};
	class rockbadmoon
	{
		name = "Creedence Clearwater - Bad Moon Rising";
		sound[] = { "@RomeMusic\RockClassic\BadMoonRising.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};
	class rockparanoid
	{
		name = "Black Sabbath - Paranoid";
		sound[] = { "@RomeMusic\RockClassic\black_sabbath___paranoid__hq_.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};
	class rockwarpigs
	{
		name = "Black Sabbath - War Pigs";
		sound[] = { "@RomeMusic\RockClassic\black_sabbath_war_pigs.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};
	class rockcelebration
	{
		name = "Kool And The Gang - Celebration";
		sound[] = { "@RomeMusic\RockClassic\Celebration.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};
	class rockmoskau
	{
		name = "Dschinghis Khan - Moskau";
		sound[] = { "@RomeMusic\RockClassic\dschinghiskhan_moskau.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};
	class rocklemontree
	{
		name = "Fool's Garden - Lemon Tree";
		sound[] = { "@RomeMusic\RockClassic\FoolsGarden-LemonTree.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};
	class rockfortunateson
	{
		name = "Fortunate Son";
		sound[] = { "@RomeMusic\RockClassic\FortunateSon.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};
	class rockseptember
	{
		name = "Earth, Wind and Fire - September";
		sound[] = { "@RomeMusic\RockClassic\September.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};
	class rockshouldistay
	{
		name = "The Clash - Should I Stay or Should I Go";
		sound[] = { "@RomeMusic\RockClassic\TheClash-ShouldIStayorShouldIGo.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};
	class rockdownunder
	{
		name = "Men At Work - DownUnder";
		sound[] = { "@RomeMusic\RockClassic\MenAtWork-DownUnder.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};

	// Pop
	class popameno
	{
		name = "ERA - Ameno";
		sound[] = { "@RomeMusic\ModernPOP\ERA_Ameno.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};
	class popboom
	{
		name = "Vengaboys - Boom Boom Boom Boom";
		sound[] = { "@RomeMusic\ModernPOP\Vengaboys-BoomBoomBoomBoom.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};
	class popdansen
	{
		name = "Caramella Girls - Caramelldansen";
		sound[] = { "@RomeMusic\ModernPOP\CaramellaGirls-CaramelldansenEnglishVersion.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};
	class pophero
	{
		name = "Bonnie Tyler - Holding Out For A Hero";
		sound[] = { "@RomeMusic\ModernPOP\Bonnie_Tyler_Holding_Out_For_A_Hero.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};
	class popkicks
	{
		name = "Foster The People - Pumped Up Kicks";
		sound[] = { "@RomeMusic\ModernPOP\PumpedUpKicks.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};
	class poprick
	{
		name = "Rick Astley - Never Gonna Give You Up";
		sound[] = { "@RomeMusic\ModernPOP\RickAstley_NeverGonnaGiveYouUp.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};
	class popbreath
	{
		name = "Berlin - Take My Breath Away";
		sound[] = { "@RomeMusic\ModernPOP\Berlin_TakeMyBreathAway.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};
	class popugot
	{
		name = "Halogen - U Got That";
		sound[] = { "@RomeMusic\ModernPOP\Halogen-UGotThat.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};
	class poprun
	{
		name = "Woodkid - Run Boy Run";
		sound[] = { "@RomeMusic\ModernPOP\RunBoyRun.ogg", 5, 1, 3000, 1, 0, 0, 0 };
		titles[] = {};
	};
};